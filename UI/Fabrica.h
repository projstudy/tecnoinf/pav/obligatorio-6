
#ifndef OBL_PROGAVA_FABRICA_H
#define OBL_PROGAVA_FABRICA_H

#include "../Controladores/ICGenerico.h"
#include "../Controladores/ICPelicula.h"

class Fabrica {
 private:
 public:
  Fabrica();

  ~Fabrica();

  ICGenerico* getICGenerico();

  ICPelicula* getICPelicula();
};

#endif  // OBL_PROGAVA_FABRICA_H
