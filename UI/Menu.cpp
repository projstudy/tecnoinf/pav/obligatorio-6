#include "Menu.h"
#include <climits>
#include <iostream>
#include <regex>
#include <stdexcept>
#include "../DataTypes/DtReloj.h"
#include "../Manejadores/Reloj.h"
#include "../Tipos/TipoFinanciera.h"
#include "../Tipos/TipoPago.h"
#include "../loader.h"
#include "Fabrica.h"

void Menu::enterParaContinuar() {
  std::cout << "\nPresione ENTER para continuar.\n";
  std::cin.ignore();
}

/**
 *
 * Funcion para limpiar pantalla
 *
 * */
void limpiar_pantalla() {
#ifdef _WIN32
  system("cls");
#else
  system("clear");
#endif
}

Menu::Menu() {
  Fabrica f = Fabrica();
  this->iGenerico = f.getICGenerico();
  this->iPelicula = f.getICPelicula();
}

Menu::~Menu() {
  delete this->iGenerico;
  delete this->iPelicula;
}

/**
 *
 * Imprime una opcion
 *
 * */
void Menu::imprimirOpcion(const std::string& opcion) {
  std::cout << opcion << std::endl;
}

/**
 *
 * Imprime una lista de opciones
 *
 * */
template <class T>
void imprimirOpciones(const std::vector<T>& opciones) {
  for (const auto& opcion : opciones) {
    std::cout << opcion << std::endl;
  }
}

/**
 *
 * Lee de la entrada standar un int entre un rango.
 *
 * */
int Menu::leerOpcion(int min, int max, const std::string& texto) {
  int n;
  bool valid = false;
  while (!valid) {
    if (texto != "") {
      std::cout << texto;
    } else {
      std::cout << "Ingrese un digito entre: " << min << " y " << max << ": ";
    }
    std::string input;
    std::getline(std::cin, input);
    try {
      size_t totalProcesado;
      n = std::stoi(input, &totalProcesado);
      if ((n < min || n > max) || totalProcesado != input.size()) {
        std::cout << "Opcion incorrecta" << std::endl;
      } else {
        valid = true;
      }
    } catch (std::invalid_argument& e) {
      std::cout << "Opcion incorrecta" << std::endl;
    }
  }
  return n;
}

/**
 *
 * Lee de la entrada standar un float
 *
 * */
float Menu::leerOpcionFloat(float min, float max, const std::string& texto) {
  float n;
  bool valid = false;
  while (!valid) {
    if (texto != "") {
      std::cout << texto;
    } else {
      std::cout << "Ingrese un digito entre: " << min << " y " << max << ": ";
    }
    std::string input;
    std::getline(std::cin, input);
    try {
      n = std::stof(input);
      if (n < min || n > max) {
        std::cout << "Opcion incorrecta, ingrese un numero entre " << min
                  << " y " << max << std::endl;
      } else {
        valid = true;
      }
    } catch (std::invalid_argument& e) {
      std::cout << "Opcion incorrecta, ingrese un numero entre " << min << " y "
                << max << std::endl;
    }
  }
  return n;
}

/**
 *
 * Lee de la entrada standar una string
 *
 * */
std::string Menu::leerOpcion(const std::string& texto) {
  if (texto != "") {
    std::cout << texto;
  }
  std::string entrada;
  std::getline(std::cin, entrada);
  return entrada;
}

/**
 *
 * Crear un DtReloj
 *
 * */

DtReloj Menu::crearDtReloj() {
  int d, m, a, hora, min;
  std::string fechaHora;
  DtReloj reloj;
  bool valid = false;
  while (!valid) {
    std::cout << "Ingrese la fecha en formato dd/mm/aaaa hh:mm" << std::endl;
    std::string fechaHora;
    std::getline(std::cin, fechaHora);
    if (5 == std::sscanf(fechaHora.c_str(), "%2d/%2d/%4d %2d:%2d", &d, &m, &a,
                         &hora, &min)) {
      if (d < 1 || d > 31) {
        std::cout << "El dia debe estar entre 1 y 31" << std::endl;
      } else if (m < 1 || m > 12) {
        std::cout << "El mes debe estar entre 1 y 12" << std::endl;
      } else if (a < 1900) {
        std::cout << "El anio debe ser mayor a 1900" << std::endl;
      } else if (hora < 0 || hora > 23) {
        std::cout << "La hora deben estar entre 0 y 23" << std::endl;
      } else if (min < 0 || min > 59) {
        std::cout << "Los minutos deben estar entre 0 y 59" << std::endl;
      } else {
        reloj = DtReloj(hora, min, d, m, a);
        valid = true;
      }
    } else {
      std::cout << "Formato invalido" << std::endl;
    }
  }
  return reloj;
}

void Menu::menuPrincipal() {
  Reloj* reloj = Reloj::getInstancia();
  bool salir = false;
  while (!salir) {
    DtReloj r = reloj->getReloj();
    std::cout << " Fecha: " << r << std::endl;
    std::cout << " Usuario: " << iGenerico->getUsuarioLogeado() << std::endl;
    std::cout << "\n\n MENU PRINCIPAL\n\n";
    std::vector<std::string> opciones{
        " [1]  Iniciar Sesion",
        " [2]  Alta Cine",
        " [3]  Alta Funcion",
        " [4]  Crear Reserva",
        " [5]  Puntuar Pelicula",
        " [6]  Comentar Pelicula",
        " [7]  Eliminar Pelicula",
        " [8]  Ver Informacion De Pelicula",
        " [9]  Ver Comentarios Y Puntajes De Pelicula",
        " [10] Setear Fecha y Hora",
        " [11] Cargar datos de prueba",
        " [0]  Salir\n"};
    imprimirOpciones(opciones);
    try {
      int x = leerOpcion(0, 11);
      switch (x) {
        case 0:
          salir = true;
          break;
        case 1:
          limpiar_pantalla();
          iniciarSesion();
          break;
        case 2:
          limpiar_pantalla();
          altaCine();
          break;
        case 3:
          limpiar_pantalla();
          altaFuncion();
          break;
        case 4:
          limpiar_pantalla();
          crearReserva();
          break;
        case 5:
          limpiar_pantalla();
          puntuarPelicula();
          break;
        case 6:
          limpiar_pantalla();
          comentarPelicula();

          break;
        case 7:
          limpiar_pantalla();
          eliminarPelicula();
          break;
        case 8:
          limpiar_pantalla();
          verInformacionDePelicula();
          break;
        case 9:
          limpiar_pantalla();
          VerComentariosYPuntajesDePelicula();
          break;
        case 10: {
          limpiar_pantalla();
          reloj = Reloj::getInstancia();
          DtReloj r = crearDtReloj();
          reloj->setReloj(r);
          enterParaContinuar();
          limpiar_pantalla();
          break;
        }
        case 11:
          datos();
          std::cout << "Datos cargados." << std::endl;
          enterParaContinuar();
          limpiar_pantalla();
          break;
      }
    } catch (std::invalid_argument& e) {
      std::cout << "\n[ERROR] " << e.what() << std::endl;
      enterParaContinuar();
      limpiar_pantalla();
    }
  }
}

/**
 *
 * Caso de uso 1
 *
 * */
void Menu::iniciarSesion() {
  imprimirOpcion("Ingrese su usuario:");
  std::string usuario = leerOpcion();
  bool deseaReIntentar = true;
  while (deseaReIntentar) {
    try {
      imprimirOpcion("Ingrese su contraseña:");
      std::string contrasenia = leerOpcion();
      iGenerico->iniciarSesion(usuario, contrasenia);
      std::cout << "Login correcto.\n";
      deseaReIntentar = false;
    } catch (std::invalid_argument& e) {
      std::cout << e.what() << std::endl;
      imprimirOpcion("Re intentar?");
      std::vector<std::string> opciones{"[1] Si", "[2] No"};
      imprimirOpciones(opciones);
      deseaReIntentar = 1 == leerOpcion(1, 2);
    }
  }
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 2
 *
 * */
void Menu::altaCine() {
  if (!iGenerico->esAdmin()) {
    throw std::invalid_argument(
        "Esta funcion esta disponible para usuarios Admin.");
  }
  std::cout << "Ingrese la direccion del cine: ";
  std::string direccionCine = leerOpcion();
  int numeroPuertaCine =
      leerOpcion(1, INT_MAX, "Ingrese el numero de puerta del cine: ");

  DtDireccion dtDireccion = DtDireccion(direccionCine, numeroPuertaCine);
  iGenerico->agregarCine(dtDireccion);
  bool quiereSeguirAgregandoSala = true;
  while (quiereSeguirAgregandoSala) {
    int capacidadSala =
        leerOpcion(1, INT_MAX, "Ingrese la capacidad de la sala: ");
    iGenerico->agregarSala(capacidadSala);
    std::cout << "\n¿Desea seguir agregando salas ?" << std::endl;
    imprimirOpciones(sino);
    if (leerOpcion(1, 2, "Respuesta: ") == 2) {
      quiereSeguirAgregandoSala = false;
    }
  }
  std::cout << "\n¿Confirma alta cine?" << std::endl;
  imprimirOpciones(sino);
  if (leerOpcion(1, 2, "Respuesta: ") == 1) {
    iGenerico->guardarCine();
  } else {
    iGenerico->cancelar();
  }
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 3
 *
 * */

void Menu::altaFuncion() {
  if (!iGenerico->esAdmin()) {
    throw std::invalid_argument(
        "Esta funcion esta disponible para usuarios Admin.");
  }
  std::cout << "LISTA DE PELICULAS\n\n";
  imprimirOpciones(iPelicula->listarTitulo());
  std::cout << "\nIngrese el nombre de la pelicula a seleccionar: ";
  std::string titulo = leerOpcion();
  std::cout << "\nCines para " << titulo << "\n" << std::endl;
  // listo los cines
  std::vector<DtCine> dtC = iGenerico->listarCines();
  // imprimo los cines
  imprimirOpciones(dtC);
  int cine = leerOpcion(0, INT_MAX, "\nSeleccione un cine (Ingrese ID): ");
  std::cout << std::endl;
  std::vector<DtSala> dtS = iPelicula->listarSalas(cine);
  imprimirOpciones(dtS);
  int sala = leerOpcion(0, INT_MAX, "Seleccione una sala (Ingrese ID): ");
  std::cout << "\nHora de inicio\n";
  DtReloj horaIni = crearDtReloj();
  std::cout << "\nHora de finalizacion\n";
  DtReloj horaFin = crearDtReloj();
  float precio = leerOpcionFloat(0, INT_MAX, "Ingrese precio de la funcion: ");
  iPelicula->crearFuncion(sala, cine, horaIni, horaFin, precio, titulo);
  std::cout << "\nFuncion creada: \nCine: " << cine << "\nSala: " << sala
            << "\nHorario de " << horaIni << " a " << horaFin
            << "\nPrecio: " << precio << "\n";
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 4
 *
 * */
void Menu::crearReserva() {
  if (!iGenerico->estaLogeado()) {
    throw std::invalid_argument(
        "Esta funcion esta disponible para usuarios registrados.");
  }
  bool quiereVerInfo = true;
  while (quiereVerInfo) {
    std::cout << "LISTA DE PELICULAS\n\n";
    imprimirOpciones(iPelicula->listarTitulo());
    imprimirOpcion("\nDesea ver informacion de alguna pelicula?");
    imprimirOpciones(sino);
    if (leerOpcion(1, 2, "Respuesta: ") == 1) {
      std::cout << "\nIngrese la pelicula: ";
      std::string titulo = leerOpcion();
      DtPeliculaExt dt = iPelicula->seleccionarPelicula(titulo);
      // imprimo tituloRecordado, poster y sinopsis
      std::cout << "\n" << dt << "\n" << std::endl;
      std::cout << "\nDesea ver informacion adicional?" << std::endl;
      imprimirOpciones(sino);
      if (leerOpcion(1, 2, "Respuesta: ") == 1) {
        std::cout << "\nLISTA DE CINES\n" << std::endl;
        // listo los cines en los que esta la pelicula
        std::vector<DtCine> dtC = iPelicula->listarCinesDePelicula(titulo);
        // imprimo los cines en los que esta la pelicula
        imprimirOpciones(dtC);
        std::cout << "\nDesea seleccionar un cine?" << std::endl;
        imprimirOpciones(sino);
        int respuesta = leerOpcion(1, 2, "Respuesta: ");
        if (respuesta == 1) {
          int cine = leerOpcion(0, INT_MAX, "Ingrese el ID de un cine: ");
          std::vector<DtFuncion> dtF = iPelicula->seleccionarCine(cine);
          imprimirOpciones(dtF);
          if (!dtF.empty()) {
            // hasta aca es igual a VetInfoPelicula
            std::cout << "\nOpciones: " << std::endl;
            std::vector<std::string> op{"[1] Continuar con la reserva.",
                                        "[2] Seleccionar otra pelicula.",
                                        "[3] Volver al menu principal"};
            imprimirOpciones(op);
            respuesta = leerOpcion(1, 3, "Respuesta: ");
            if (respuesta == 1) {
              int f =
                  leerOpcion(1, INT_MAX, "\nSeleccione la ID de la funcion: ");
              iPelicula->seleccionarFuncion(f);
              auto iterator = dtF.begin();
              int maxCantidadAsientos = 0;
              bool buscar = true;
              while (iterator != dtF.end() && buscar) {
                if ((*iterator).getId() == f) {
                  maxCantidadAsientos = (*iterator).getAsientosLibres();
                  buscar = false;
                } else {
                  iterator++;
                }
              }
              int cantAsientos =
                  leerOpcion(1, maxCantidadAsientos,
                             "Ingrese la cantidad de asientos (max: " +
                                 std::to_string(maxCantidadAsientos) + ")");
              std::cout << "\nTipo de pago" << std::endl;
              std::cout << "[1] Debito.\n[2] Credito." << std::endl;
              int tipoPago = leerOpcion(1, INT_MAX, "Respuesta: ");
              if (tipoPago == 1) {
                std::string banco =
                    leerOpcion("\nIngrese el nombre del banco: ");
                float precioTotalDebito =
                    iPelicula->verPrecioTotalDebito(cantAsientos);
                std::cout << "Importe total = " << precioTotalDebito
                          << std::endl;
                std::cout << "¿Confirma reserva?" << std::endl;
                imprimirOpciones(sino);
                int confirmaDebito = leerOpcion(1, 2, "Respuesta: ");
                if (confirmaDebito == 1) {
                  iPelicula->crearReservaDebito(cantAsientos, banco);
                  std::cout << "Reserva creada" << std::endl;
                } else {
                  quiereVerInfo = false;
                }
                enterParaContinuar();
                limpiar_pantalla();
              } else if (tipoPago == 2) {
                std::cout << "Elija financiera." << std::endl;
                std::vector<std::string> financiera{"[1] Visa", "[2] Master",
                                                    "[3] Cabal"};
                imprimirOpciones(financiera);
                respuesta = leerOpcion(1, 3, "Respuesta: ");
                auto tf = TipoFinanciera(respuesta - 1);
                auto descuento = iGenerico->getDescuentoFinanciera(tf);
                std::cout << "\tDescuento: " << descuento << "%" << std::endl;
                float precioTotalCredito =
                    iPelicula->verPrecioTotalCredito(cantAsientos, tf);
                std::cout << "Importe total = " << precioTotalCredito
                          << std::endl;
                std::cout << "Confirma reserva?" << std::endl;
                imprimirOpciones(sino);
                int confirmaCredito = leerOpcion(1, 2, "Respuesta: ");
                if (confirmaCredito == 1) {
                  iPelicula->crearReservaCredito(cantAsientos);
                  std::cout << "Reserva creada." << std::endl;
                } else {
                  quiereVerInfo = false;
                }
                enterParaContinuar();
                limpiar_pantalla();
              }
            } else if (respuesta == 3) {
              quiereVerInfo = false;
            } else {
              limpiar_pantalla();
            }
          } else {
            std::cout << "\nNo hay funciones disponibles." << std::endl;
            enterParaContinuar();
            limpiar_pantalla();
          }
        }
      }
    } else {
      quiereVerInfo = false;
    }
  }
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 5
 *
 * */
void Menu::puntuarPelicula() {
  if (!iGenerico->estaLogeado()) {
    throw std::invalid_argument(
        "Esta funcion esta disponible para usuarios registrados.");
  }
  imprimirOpciones(iPelicula->listarTitulo());
  std::string titulo =
      leerOpcion("\nIngrese el nombre de pelicula que desea puntuar: ");
  if (iPelicula->estaPuntuada(titulo)) {
    std::cout << "\nPuntaje anterior: " << iPelicula->verPuntaje() << std::endl;
    imprimirOpcion("\n¿Desea cambiarlo?\n");
    std::vector<std::string> opciones{"[1] Si", "[2] No"};
    imprimirOpciones(opciones);
    if (leerOpcion(1, 2, "Respuesta: ") == 1) {
      float puntaje =
          leerOpcionFloat(1, 5, "\nIngrese un puntaje entre 1 y 5: ");
      iPelicula->puntuar(puntaje);
      std::cout << "\nPuntaje actualizado." << std::endl;
    }
  } else {
    float puntaje = leerOpcionFloat(1, 5, "Ingrese un puntaje entre 1 y 5: ");
    iPelicula->puntuar(puntaje);
    std::cout << "\nPuntaje ingresado." << std::endl;
  }
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 6
 *
 * */
void Menu::comentarPelicula() {
  if (!iGenerico->estaLogeado()) {
    throw std::invalid_argument(
        "Esta funcion esta disponible para usuarios registrados.");
  }
  imprimirOpciones(iPelicula->listarTitulo());
  std::string titulo =
      leerOpcion("\nIngrese el nombre de pelicula que desea comentar: ");
  std::cout << std::endl;
  auto dt = iPelicula->listarComentariosPeliculas(titulo);
  imprimirOpciones(dt);

  bool quiereSeguirComentando = true;
  while (quiereSeguirComentando) {
    imprimirOpcion("\n[1] Nuevo comentario\n[2] Responder comentario");
    int opcion = leerOpcion(1, 2, "Respuesta: ");
    if (opcion == 1) {
      std::string comentario = leerOpcion("\nIngrese el nuevo comentario: ");
      iPelicula->ingresarComentario(comentario, titulo);
    } else {
      int idComentario = leerOpcion(1, INT_MAX, "\nElija un comentario: ");
      std::string comentario = leerOpcion("Ingrese la respuesta: ");
      iPelicula->responderComentario(idComentario, comentario);
    }
    std::cout << "\n¿Desea seguir comentando esta pelicula?" << std::endl;
    imprimirOpciones(sino);
    if (leerOpcion(1, 2, "Respuesta: ") == 2) {
      quiereSeguirComentando = false;
    }
  }
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 7
 *
 * */

void Menu::eliminarPelicula() {
  if (!iGenerico->esAdmin()) {
    throw std::invalid_argument(
        "Esta funcion esta disponible para usuarios Admin.");
  }
  imprimirOpciones(iPelicula->listarTitulo());
  std::string titulo =
      leerOpcion("Ingrese el nombre de pelicula que desea eliminar: ");
  imprimirOpcion("\n¿Desea confirmar eliminar pelicula?");
  imprimirOpciones(sino);
  int confirmar = leerOpcion(1, 2, "Respuesta: ");

  if (confirmar == 1) {
    iPelicula->confirmarEliminarPelicula(titulo);
    std::cout << "\nPelicula eliminada." << std::endl;
  }
  enterParaContinuar();
  limpiar_pantalla();
}

/**
 *
 * Caso de uso 8
 *
 * */
void Menu::verInformacionDePelicula() {
  bool quiereVerInfo = true;
  while (quiereVerInfo) {
    std::cout << "LISTA DE PELICULAS\n\n";
    imprimirOpciones(iPelicula->listarTitulo());
    imprimirOpcion("\nDesea ver informacion de alguna pelicula?");
    imprimirOpciones(sino);
    if (leerOpcion(1, 2, "Respuesta: ") == 1) {
      std::cout << "\nIngrese la pelicula: ";
      std::string titulo = leerOpcion();
      DtPeliculaExt dt = iPelicula->seleccionarPelicula(titulo);
      // imprimo tituloRecordado, poster y sinopsis
      std::cout << "\n" << dt << std::endl << std::endl;
      std::cout << "\nDesea ver informacion adicional?" << std::endl;
      imprimirOpciones(sino);
      if (leerOpcion(1, 2, "Respuesta: ") == 1) {
        std::cout << "\nLISTA DE CINES" << std::endl;
        // listo los cines en los que esta la pelicula
        std::vector<DtCine> dtC = iPelicula->listarCinesDePelicula(titulo);
        // imprimo los cines en los que esta la pelicula
        imprimirOpciones(dtC);
        std::cout << "\nDesea seleccionar un cine?" << std::endl;
        imprimirOpciones(sino);
        if (leerOpcion(1, 2, "Respuesta: ") == 1) {
          int cine = leerOpcion(0, INT_MAX, "Ingrese el ID de un cine: ");
          std::vector<DtFuncion> dtF = iPelicula->seleccionarCine(cine);
          imprimirOpciones(dtF);
        }
      }
      std::cout << "\nDesea ver informacion de otra pelicula?" << std::endl;
      std::vector<std::string> op{"[1] Si", "[2] No"};
      imprimirOpciones(op);
      if (leerOpcion(1, 2, "Respuesta: ") == 2) {
        iPelicula->cancelarVerInfoPelicula();
        quiereVerInfo = false;
      }
    } else {
      quiereVerInfo = false;
    }
    limpiar_pantalla();
  }
}

/**
 *
 * Caso de uso 9
 *
 * */
void Menu::VerComentariosYPuntajesDePelicula() {
  std::cout << "LISTA DE PELICULAS\n\n";

  imprimirOpciones(iPelicula->listarPeliculas());

  std::string titulo = leerOpcion(
      "\n Ingrese el nombre de pelicula que desea ver sus comentarios y "
      "puntaje: ");

  auto datos = iPelicula->verComentariosPuntajes(titulo);
  std::cout << "\n \n" << std::endl;
  std::cout << titulo << std::endl;
  std::cout << datos << std::endl;

  enterParaContinuar();
  limpiar_pantalla();
}
