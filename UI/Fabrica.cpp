

#include "Fabrica.h"
#include "../Controladores/CGenerico.h"
#include "../Controladores/CPelicula.h"

Fabrica::Fabrica() {}

ICPelicula* Fabrica::getICPelicula() {
  return new CPelicula();
}

ICGenerico* Fabrica::getICGenerico() {
  return new CGenerico();
}

Fabrica::~Fabrica() {}
