//
// Created by e974616 on 6/7/2019.
//
#include <string>
#include <vector>
#include "../Controladores/ICGenerico.h"
#include "../Controladores/ICPelicula.h"

#ifndef OBL_PROGAVA_MENU_H
#define OBL_PROGAVA_MENU_H

class Menu {
  template <class T>
  friend void imprimirOpciones(const std::vector<T>&);

 private:
  ICGenerico* iGenerico;
  ICPelicula* iPelicula;
  std::vector<std::string> sino{"[1] Si", "[2] No"};

 public:
  Menu();

  virtual ~Menu();

  void imprimirOpcion(const std::string&);

  int leerOpcion(int, int, const std::string& text = "");

  float leerOpcionFloat(float, float, const std::string& text = "");

  std::string leerOpcion(const std::string& text = "");

  //
  void menuPrincipal();

  void iniciarSesion();

  void altaFuncion();

  void eliminarPelicula();
  void crearReserva();
  void altaCine();
  void puntuarPelicula();
  void comentarPelicula();

  void verInformacionDePelicula();

  void enterParaContinuar();

  void VerComentariosYPuntajesDePelicula();

  DtReloj crearDtReloj();
};

#endif  // OBL_PROGAVA_MENU_H
