
#ifndef OBL_PROGAVA_ICGENERICO_H
#define OBL_PROGAVA_ICGENERICO_H

#include <string>
#include <vector>
#include "../DataTypes/DtCine.h"
#include "../DataTypes/DtDireccion.h"
#include "../Tipos/TipoFinanciera.h"

class ICGenerico {
 public:
  virtual ~ICGenerico() {}

  virtual void iniciarSesion(std::string, std::string) = 0;

  virtual void agregarCine(DtDireccion) = 0;

  virtual void agregarSala(int) = 0;

  virtual void guardarCine() = 0;

  virtual void cancelar() = 0;

  virtual float getDescuentoFinanciera(TipoFinanciera) = 0;

  virtual std::vector<DtCine> listarCines() = 0;

  virtual std::string getUsuarioLogeado() = 0;

  virtual bool estaLogeado() = 0;

  virtual bool esAdmin() = 0;
};

#endif  // OBL_PROGAVA_ICGENERICO_H
