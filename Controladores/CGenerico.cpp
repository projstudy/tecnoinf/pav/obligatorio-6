#include "CGenerico.h"
#include "../Clases/Cine.h"

CGenerico::CGenerico() {}

CGenerico::~CGenerico() {}

void CGenerico::iniciarSesion(std::string nickname, std::string contrasenia) {
  try {
    Usuario* u = manejador->buscarUsuario(nickname);
    if (u->comprobarContrasenia(contrasenia)) {
      sesion->setSesionUsuario(u);
    } else {
      throw std::invalid_argument("Contrasenia invalida");
    }
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CGenerico::agregarCine(DtDireccion direccion) {
  Cine* c = new Cine(direccion);
  this->cineRecordado = c;
}

void CGenerico::agregarSala(int capacidad) {
  auto sala = new Sala(capacidad);
  this->cineRecordado->agregarSala(sala);
}

void CGenerico::guardarCine() {
  manejador->agregarCine(this->cineRecordado);
}

void CGenerico::cancelar() {
  delete cineRecordado;
  this->cineRecordado = nullptr;
}

float CGenerico::getDescuentoFinanciera(TipoFinanciera tFinanciera) {
  try {
    return manejador->buscarFinanciera(tFinanciera).getDescuento();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

std::vector<DtCine> CGenerico::listarCines() {
  std::vector<DtCine> vecCines;
  auto cines = manejador->getCines();
  for (auto cine : cines) {
    vecCines.push_back(cine->getDtCine());
  }
  return vecCines;
}

std::string CGenerico::getUsuarioLogeado() {
  auto usuario = sesion->getSesionUsuario();
  if (usuario) {
    return usuario->getNickname();
  }
  return "Invitado";
}

bool CGenerico::esAdmin() {
  auto usuario = sesion->getSesionUsuario();
  return (usuario && usuario->esAdmin());
}

bool CGenerico::estaLogeado() {
  auto usuario = sesion->getSesionUsuario();
  return usuario != nullptr;
}
