#include "CPelicula.h"
#include "../Clases/Credito.h"
#include "../Clases/Debito.h"
#include "../Clases/Reserva.h"
#include "../DataTypes/DtReloj.h"

CPelicula::CPelicula() {}

CPelicula::~CPelicula() {}

// Metodos

std::vector<std::string> CPelicula::listarTitulo() {
  auto peliculas = manejador->getPelicula();
  std::vector<std::string> resultado;
  for (auto& p : peliculas) {
    resultado.push_back(p->getTitulo());
  }
  return resultado;
}

std::vector<DtCine> CPelicula::listarCinesDePelicula(std::string titulo) {
  this->tituloRecordado = titulo;
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    return pelicula->listarCines();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::cancelarVerInfoPelicula() {
  this->tituloRecordado = "";
}

std::vector<DtFuncion> CPelicula::seleccionarCine(int id) {
  std::vector<DtFuncion> resultado;

  try {
    auto cine = manejador->buscarCine(id);
    auto funciones = cine->getDtFunciones();

    auto pelicula = manejador->buscarPelicula(this->tituloRecordado);

    auto dtReloj = reloj->getReloj();

    for (auto dtFuncion : funciones) {
      int idFuncion = dtFuncion.getId();
      if (pelicula->existeFuncion(idFuncion)) {
        if (dtReloj < dtFuncion.getHoraInicio()) {
          resultado.push_back(dtFuncion);
        }
      }
    }
  } catch (std::invalid_argument& e) {
    throw e;
  }
  return resultado;
}

void CPelicula::seleccionarFuncion(int id) {
  try {
    this->funcionRecordada = manejador->buscarFuncion(id);
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::confirmarEliminarPelicula(std::string titulo) {
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    manejador->eliminarPelicula(titulo);
    delete pelicula;
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::cancelarReserva() {
  this->funcionRecordada = nullptr;
}

float CPelicula::verPrecioTotalCredito(int cantAsientos,
                                       TipoFinanciera tFinanciera) {
  float precio = this->funcionRecordada->getPrecio();
  try {
    this->financieraRecordada = manejador->buscarFinanciera(tFinanciera);
    float descuento = this->financieraRecordada.getDescuento();
    costoTotalRecordado =
        (precio * cantAsientos) - (precio * cantAsientos) * (descuento / 100);
    return costoTotalRecordado;
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::crearReservaCredito(int cantAsientos) {
  auto usuario = sesion->getSesionUsuario();
  auto credito = new Credito(cantAsientos, costoTotalRecordado, usuario,
                             financieraRecordada);
  credito->setUsuairo(usuario);
  this->funcionRecordada->addReserva(credito);
  // olvida la funcion
  this->funcionRecordada = nullptr;
}

float CPelicula::verPrecioTotalDebito(int cantAsientos) {
  float precio = funcionRecordada->getPrecio();
  this->costoTotalRecordado = precio * cantAsientos;
  return this->costoTotalRecordado;
}

void CPelicula::crearReservaDebito(int cantAsientos, std::string banco) {
  auto usuario = sesion->getSesionUsuario();
  auto debito = new Debito(cantAsientos, costoTotalRecordado, usuario, banco);
  debito->setUsuairo(usuario);
  this->funcionRecordada->addReserva(debito);
  this->funcionRecordada = nullptr;
}

bool CPelicula::estaPuntuada(std::string titulo) {
  this->tituloRecordado = titulo;
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    return pelicula->estaPuntuada();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

float CPelicula::verPuntaje() {
  try {
    auto pelicula = manejador->buscarPelicula(tituloRecordado);
    return pelicula->verPuntaje();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::puntuar(float puntaje) {
  try {
    auto pelicula = manejador->buscarPelicula(tituloRecordado);
    pelicula->puntuar(puntaje);
  } catch (std::invalid_argument& e) {
    throw e;
  }
  this->tituloRecordado = "";
}

std::vector<DtPelicula> CPelicula::listarPeliculas() {
  auto vectorPeliculas = manejador->getPelicula();
  std::vector<DtPelicula> resultado;
  for (const auto pel : vectorPeliculas) {
    resultado.push_back(pel->getDtPelicula());
  }
  return resultado;
}

DtPeliculaExt CPelicula::seleccionarPelicula(std::string titulo) {
  this->tituloRecordado = titulo;
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    return pelicula->getDtPeliculaExt();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

DtComentarioPuntaje CPelicula::verComentariosPuntajes(std::string titulo) {
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    return pelicula->getDtComentarioPuntaje();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

std::vector<DtComentario> CPelicula::listarComentariosPeliculas(
    std::string titulo) {
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    return pelicula->getDtComentarios();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::ingresarComentario(std::string texto, std::string titulo) {
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    return pelicula->ingresarComentario(texto);
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::responderComentario(int idComentario, std::string comentario) {
  auto u = sesion->getSesionUsuario();
  try {
    auto comentarioPadre = manejador->buscarComentario(idComentario);
    auto com = new Comentario(comentario, u);
    comentarioPadre->addComentario(com);
    manejador->agregarComentario(com);
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

std::vector<DtSala> CPelicula::listarSalas(int idCine) {
  try {
    auto cine = manejador->buscarCine(idCine);
    return cine->getListaDtSalas();
  } catch (std::invalid_argument& e) {
    throw e;
  }
}

void CPelicula::crearFuncion(int idSala,
                             int IdCine,
                             DtReloj horaInicio,
                             DtReloj horaFin,
                             float precio,
                             std::string titulo) {
  if (horaFin < horaInicio) {
    throw std::invalid_argument(
        "La hora fin no puede ser menor a la hora inicio");
  }
  auto horaReloj = reloj->getReloj();
  if (horaInicio < horaReloj) {
    throw std::invalid_argument("La funcion no se puede crear en el pasado");
  }
  try {
    auto pelicula = manejador->buscarPelicula(titulo);
    auto cine = manejador->buscarCine(IdCine);
    auto dt = cine->getDtSala(idSala);
    auto iterator = dt.getHoras().begin();
    while (iterator != dt.getHoras().end()) {
      auto par = *iterator;
      auto inicio = par.first;
      auto fin = par.second;
      if ((horaInicio < inicio && inicio < horaInicio) ||
          (horaInicio < fin && fin < horaFin) ||
          (inicio < horaInicio && horaFin < fin)) {
        throw std::invalid_argument("Error, sala ocupada.");
      }
      iterator++;
    }

    int idFuncion = cine->crearFuncion(idSala, horaInicio, horaFin, precio);
    auto funcionNueva = manejador->buscarFuncion(idFuncion);

    // nueva funcion de la pelicula
    pelicula->agregarFuncion(funcionNueva);
    // registro la pelicula en el cine
    pelicula->agregarCine(cine);

  } catch (std::invalid_argument& e) {
    throw e;
  }
}
