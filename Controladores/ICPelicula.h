

#ifndef OBL_PROGAVA_ICPELICULA_H
#define OBL_PROGAVA_ICPELICULA_H

#include <string>
#include "../Clases/Financiera.h"
#include "../Clases/Pelicula.h"
#include "../DataTypes/DtCine.h"
#include "../DataTypes/DtSala.h"
#include "../Tipos/TipoFinanciera.h"
#include "../Tipos/TipoPago.h"

class ICPelicula {
 public:
  virtual ~ICPelicula() {}

  virtual std::vector<std::string> listarTitulo() = 0;

  virtual std::vector<DtCine> listarCinesDePelicula(std::string) = 0;

  virtual void cancelarVerInfoPelicula() = 0;

  virtual std::vector<DtFuncion> seleccionarCine(int) = 0;

  virtual void confirmarEliminarPelicula(std::string) = 0;

  virtual void seleccionarFuncion(int) = 0;

  virtual void cancelarReserva() = 0;

  virtual float verPrecioTotalCredito(int, TipoFinanciera) = 0;

  virtual float verPrecioTotalDebito(int) = 0;

  virtual void crearReservaCredito(int) = 0;

  virtual void crearReservaDebito(int, std::string) = 0;

  virtual bool estaPuntuada(std::string) = 0;

  virtual float verPuntaje() = 0;

  virtual void puntuar(float) = 0;

  virtual std::vector<DtPelicula> listarPeliculas() = 0;

  virtual DtComentarioPuntaje verComentariosPuntajes(std::string) = 0;

  virtual std::vector<DtComentario> listarComentariosPeliculas(std::string) = 0;

  virtual void ingresarComentario(std::string, std::string) = 0;

  virtual void responderComentario(int, std::string) = 0;

  virtual std::vector<DtSala> listarSalas(int) = 0;

  virtual void crearFuncion(int idSala,
                            int IdCine,
                            DtReloj horaIncio,
                            DtReloj horaFin,
                            float precio,
                            std::string titulo) = 0;
  virtual DtPeliculaExt seleccionarPelicula(std::string) = 0;
};

#endif  // OBL_PROGAVA_ICPELICULA_H
