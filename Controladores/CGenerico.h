

#ifndef OBL_PROGAVA_CGENERICO_H
#define OBL_PROGAVA_CGENERICO_H

#include <string>
#include "../Clases/Cine.h"
#include "../DataTypes/DtDireccion.h"
#include "../Manejadores/ManejadorGenerico.h"
#include "../Manejadores/Sesion.h"
#include "../Tipos/TipoFinanciera.h"
#include "ICGenerico.h"

class CGenerico : public ICGenerico {
 private:
  Manejador* manejador = Manejador::getInstancia();
  ManejadorSesion* sesion = ManejadorSesion::getInstancia();
  Cine* cineRecordado;

 public:
  CGenerico();

  ~CGenerico();

  void iniciarSesion(std::string, std::string);

  void agregarCine(DtDireccion);

  void agregarSala(int);

  void guardarCine();

  void cancelar();

  float getDescuentoFinanciera(TipoFinanciera f);

  std::string getUsuarioLogeado();

  bool estaLogeado();

  bool esAdmin();

  std::vector<DtCine> listarCines();
};

#endif  // OBL_PROGAVA_CGENERICO_H
