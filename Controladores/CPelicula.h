
#ifndef OBL_PROGAVA_CPELICULA_H
#define OBL_PROGAVA_CPELICULA_H

#include <string>
#include <vector>
#include "../Clases/Financiera.h"
#include "../Clases/Pelicula.h"
#include "../DataTypes/DtCine.h"
#include "../DataTypes/DtComentarioPuntaje.h"
#include "../DataTypes/DtPelicula.h"
#include "../Manejadores/ManejadorGenerico.h"
#include "../Manejadores/Reloj.h"
#include "../Manejadores/Sesion.h"
#include "../Tipos/TipoFinanciera.h"
#include "../Tipos/TipoPago.h"
#include "ICPelicula.h"

class CPelicula : public ICPelicula {
 private:
  std::string tituloRecordado;
  Financiera financieraRecordada;
  Funcion* funcionRecordada;
  float costoTotalRecordado;
  ManejadorSesion* sesion = ManejadorSesion::getInstancia();
  Manejador* manejador = Manejador::getInstancia();
  Reloj* reloj = Reloj::getInstancia();

 public:
  CPelicula();

  ~CPelicula();

  std::vector<std::string> listarTitulo();

  std::vector<DtCine> listarCinesDePelicula(std::string titulo);

  void cancelarVerInfoPelicula();

  std::vector<DtFuncion> seleccionarCine(int);

  void confirmarEliminarPelicula(std::string);

  void seleccionarFuncion(int);

  void cancelarReserva();

  float verPrecioTotalCredito(int, TipoFinanciera);

  float verPrecioTotalDebito(int);

  void crearReservaCredito(int);

  void crearReservaDebito(int, std::string);

  bool estaPuntuada(std::string);

  float verPuntaje();

  void puntuar(float);

  std::vector<DtPelicula> listarPeliculas();

  DtComentarioPuntaje verComentariosPuntajes(std::string);

  std::vector<DtComentario> listarComentariosPeliculas(std::string);

  void ingresarComentario(std::string, std::string);

  void responderComentario(int, std::string);

  std::vector<DtSala> listarSalas(int idCine);

  void crearFuncion(int idSala,
                    int IdCine,
                    DtReloj horaIncio,
                    DtReloj horaFin,
                    float precio,
                    std::string titulo);

  DtPeliculaExt seleccionarPelicula(std::string);
};

#endif  // OBL_PROGAVA_CPELICULA_H
