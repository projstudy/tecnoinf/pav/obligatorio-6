//
// Created by mcastro on 03/06/2019.
//

#ifndef OBL_PROGAVA_TIPOFINANCIERA_H
#define OBL_PROGAVA_TIPOFINANCIERA_H

typedef enum { Visa, Master, Cabal } TipoFinanciera;

#endif  // OBL_PROGAVA_TIPOFINANCIERA_H
