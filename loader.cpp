#include <vector>
#include "Clases/Cine.h"
#include "Clases/Pelicula.h"
#include "Clases/Usuario.h"
#include "Controladores/CPelicula.h"
#include "Manejadores/Sesion.h"

auto cp = new CPelicula();
auto manejador = Manejador::getInstancia();
auto sesion = ManejadorSesion::getInstancia();

void datos() {
  std::vector<std::string> nombres{"juaco",   "admin", "mike",    "gonzalo",
                                   "mariano", "juan",  "mathias", "franco"};
  std::vector<std::string> contrasenias{"juaco",   "admin",   "mike",
                                        "gonzalo", "mariano", "juan",
                                        "mathias", "franco"};
  std::vector<std::string> imagen{"maraco.png", "admin.png", "hola.jpg",
                                  "hola.jpg",   "hola.jpg",  "hola.jpg",
                                  "hola.jpg",   "hola.jpg"};
  Usuario* u;
  for (int i = 0; i < 8; ++i) {
    u = new Usuario(nombres[i], imagen[i], contrasenias[i]);
    manejador->agregarMUsuario(u);
  }

  sesion->setSesionUsuario(u);
  /*
   *
   *
   * */

  auto p1 = new Pelicula(
      "Glass", "https://somesite/glass.png",
      "Security guard David Dunn uses his supernatural abilities to track "
      "Kevin Wendell Crumb, a disturbed man who has twenty-four personalities.",
      0);
  auto p2 = new Pelicula("Captain Marvel", "https://somesite/cmarvel.png",
                         "Carol Danvers becomes one of the universe's most "
                         "powerful heroes when Earth is caught in the middle "
                         "of a galactic war between two alien races.",
                         0);
  auto p3 =
      new Pelicula("Shazam!", "https://somesime/shazam.png",
                   "We all have a superhero inside us, it just takes a bit of "
                   "magic to bring it out. In Billy Batson's case, by shouting "
                   "out one word - SHAZAM. - this streetwise fourteen-year-old "
                   "foster kid can turn into the grown-up superhero Shazam.",
                   0);
  auto p4 = new Pelicula("Rocketman", "https://somesite/elton.png",
                         "A musical fantasy about the fantastical human story "
                         "of Elton John's breakthrough years.",
                         0);
  auto p5 = new Pelicula(
      "Aladdin", "https://somesite/aladdin.png",
      " A kind-hearted street urchin and a power-hungry Grand Vizier vie for a "
      "magic lamp that has the power to make their deepest wishes come true.",
      0);
  auto p6 = new Pelicula(
      "The Lion King", "https://somesite/sad90s.png",
      "After the murder of his father, a young lion prince flees his kingdom "
      "only to learn the true meaning of responsibility and bravery.",
      0);
  auto p7 = new Pelicula(
      "Godzilla: King of the Monsters", "https://somesite/fatdragon.png",
      "The crypto-zoological agency Monarch faces off against a battery of "
      "god-sized monsters, including the mighty Godzilla, who collides with "
      "Mothra, Rodan, and his ultimate nemesis, the three-headed King "
      "Ghidorah.",
      0);
  auto p8 =
      new Pelicula("Hellboy", "https://somesite/reddude.png",
                   " Based on the graphic novels by Mike Mignola, Hellboy, "
                   "caught between the worlds of the supernatural and human, "
                   "battles an ancient sorceress bent on revenge.",
                   0);

  manejador->agregarPelicula(p1);
  manejador->agregarPelicula(p2);
  manejador->agregarPelicula(p3);
  manejador->agregarPelicula(p4);
  manejador->agregarPelicula(p5);
  manejador->agregarPelicula(p6);
  manejador->agregarPelicula(p7);
  manejador->agregarPelicula(p8);

  /*
   *
   *
   * */

  DtDireccion dt = DtDireccion("Av. Dr. Luis Alberto de Herrera", 1290);
  auto c = new Cine(dt);
  auto s = new Sala(20);

  auto f1 = new Funcion(250, DtReloj(9, 0, 10, 10, 2019),
                        DtReloj(11, 0, 10, 10, 2019));
  auto f2 = new Funcion(250, DtReloj(11, 30, 10, 10, 2019),
                        DtReloj(13, 30, 10, 10, 2019));
  auto f3 = new Funcion(250, DtReloj(14, 00, 10, 10, 2019),
                        DtReloj(16, 00, 10, 10, 2019));
  auto f4 = new Funcion(250, DtReloj(16, 15, 10, 10, 2019),
                        DtReloj(18, 30, 10, 10, 2019));

  c->agregarSala(s);

  p1->agregarFuncion(f1);
  p2->agregarFuncion(f2);
  p3->agregarFuncion(f3);
  p4->agregarFuncion(f4);

  p1->agregarCine(c);
  p2->agregarCine(c);
  p3->agregarCine(c);
  p4->agregarCine(c);

  s->agregarFuncion(f1);
  s->agregarFuncion(f2);
  s->agregarFuncion(f3);
  s->agregarFuncion(f4);

  manejador->agregarFuncion(f1);
  manejador->agregarFuncion(f2);
  manejador->agregarFuncion(f3);
  manejador->agregarFuncion(f4);
  manejador->agregarCine(c);

  /*
   *
   *
   * */
  dt = DtDireccion("Ejido", 1377);
  c = new Cine(dt);
  s = new Sala(15);
  c->agregarSala(s);

  f1 = new Funcion(250, DtReloj(12, 15, 25, 12, 2019),
                   DtReloj(14, 15, 25, 12, 2019));
  f2 = new Funcion(350, DtReloj(15, 0, 25, 12, 2019),
                   DtReloj(17, 30, 25, 12, 2019));
  f3 = new Funcion(150, DtReloj(18, 15, 25, 12, 2019),
                   DtReloj(20, 00, 25, 12, 2019));
  f4 = new Funcion(450, DtReloj(20, 15, 25, 12, 2019),
                   DtReloj(21, 45, 25, 12, 2019));

  p1->agregarFuncion(f1);
  p2->agregarFuncion(f2);
  p3->agregarFuncion(f3);
  p4->agregarFuncion(f4);

  p1->agregarCine(c);
  p2->agregarCine(c);
  p3->agregarCine(c);
  p4->agregarCine(c);

  s->agregarFuncion(f1);
  s->agregarFuncion(f2);
  s->agregarFuncion(f3);
  s->agregarFuncion(f4);

  manejador->agregarFuncion(f1);
  manejador->agregarFuncion(f2);
  manejador->agregarFuncion(f3);
  manejador->agregarFuncion(f4);

  s = new Sala(20);
  c->agregarSala(s);
  f1 = new Funcion(335, DtReloj(9, 0, 7, 7, 2019), DtReloj(11, 0, 7, 7, 2019));
  f2 = new Funcion(335, DtReloj(12, 0, 7, 7, 2019), DtReloj(14, 0, 7, 7, 2019));
  f3 = new Funcion(335, DtReloj(16, 0, 7, 7, 2019), DtReloj(18, 0, 7, 7, 2019));
  f4 = new Funcion(335, DtReloj(20, 0, 7, 7, 2019), DtReloj(13, 0, 7, 7, 2019));

  s->agregarFuncion(f1);
  s->agregarFuncion(f2);
  s->agregarFuncion(f3);
  s->agregarFuncion(f4);
  p1->agregarFuncion(f1);
  p1->agregarFuncion(f2);
  p1->agregarFuncion(f3);
  p1->agregarFuncion(f4);

  manejador->agregarFuncion(f1);
  manejador->agregarFuncion(f2);
  manejador->agregarFuncion(f3);
  manejador->agregarFuncion(f4);

  manejador->agregarCine(c);

  auto puntaje = new Puntaje(5, manejador->buscarUsuario("franco"));
  p1->agregarPuntaje(puntaje);
  puntaje = new Puntaje(2.5, manejador->buscarUsuario("juan"));
  p1->agregarPuntaje(puntaje);
  puntaje = new Puntaje(1.3, manejador->buscarUsuario("mike"));
  p1->agregarPuntaje(puntaje);
  puntaje = new Puntaje(4.2, manejador->buscarUsuario("mathias"));
  p1->agregarPuntaje(puntaje);

  puntaje = new Puntaje(4, u);
  p2->agregarPuntaje(puntaje);
  puntaje = new Puntaje(3.5, u);
  p3->agregarPuntaje(puntaje);

  auto com = new Comentario("He visto mejores...", u);
  manejador->agregarComentario(com);
  p1->agregarComentario(com);

  sesion->setSesionUsuario(manejador->buscarUsuario("mike"));

  cp->responderComentario(com->getId(), "La precuela esta mejor!");

  sesion->setSesionUsuario(manejador->buscarUsuario("gonzalo"));
  cp->responderComentario(2, "Que sabras salame?");

  sesion->setSesionUsuario(manejador->buscarUsuario("mariano"));
  cp->responderComentario(3, "Aguante el foro bardo!!!");

  sesion->setSesionUsuario(manejador->buscarUsuario("juan"));
  cp->responderComentario(com->getId(), "El Zorro esta mucho mejor!");

  com = new Comentario("No la pasan en español! :(",
                       manejador->buscarUsuario("juaco"));
  manejador->agregarComentario(com);
  p1->agregarComentario(com);

  auto financiera = Financiera(Cabal, 32.2);
  manejador->agregarFinanciera(financiera);
  financiera = Financiera(Visa, 17.2);
  manejador->agregarFinanciera(financiera);
  financiera = Financiera(Master, 5);
  manejador->agregarFinanciera(financiera);
  // logout
  sesion->setSesionUsuario(nullptr);
  manejador->buscarUsuario("admin")->setAdmin(true);
}
