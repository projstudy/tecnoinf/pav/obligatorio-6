//
// Created by mcastro on 04/06/2019.
//

#ifndef OBL_PROGAVA_DTPUNTAJE_H
#define OBL_PROGAVA_DTPUNTAJE_H

#include <string>

class DtPuntaje {
  friend std::ostream& operator<<(std::ostream&, const DtPuntaje&);

 private:
  float puntaje;
  std::string nickname;

 public:
  DtPuntaje();

  DtPuntaje(float puntaje, const std::string& nickname);

  float getPuntaje() const;

  const std::string& getNickname() const;

  virtual ~DtPuntaje();
};

#endif  // OBL_PROGAVA_DTPUNTAJE_H
