
#include "DtSala.h"
#include <stdexcept>

using namespace std;

// Constructor por defecto
DtSala::DtSala() {
  this->id = 0;
  this->capacidad = 0;
}

// Constructor con parametros
DtSala::DtSala(int id,
               int capacidad,
               const std::vector<std::pair<DtReloj, DtReloj>>& horas) {
  if (id < 0) {
    throw invalid_argument("Numero invalido\n");
  } else {
    this->id = id;
    this->capacidad = capacidad;
    this->horas = horas;
  }
}

// Getters
int DtSala::getId() const {
  return id;
}

int DtSala::getCapacidad() const {
  return capacidad;
}

// Destructor
DtSala::~DtSala() {}

// Operacion
std::vector<std::pair<DtReloj, DtReloj>> DtSala::getHoras() const {
  return this->horas;
}

// Sobrecarga

std::ostream& operator<<(std::ostream& salida, const DtSala& sala) {
  salida << "- Id: " << sala.getId() << "\n  Capacidad: " << sala.getCapacidad()
         << " personas\n";
  if (!sala.horas.empty()) {
    salida << "  Sala Ocupada:\n";
  }
  for (const auto& parHora : sala.horas) {
    salida << "    De: " << parHora.first << " a " << parHora.second << endl;
  }
  return salida;
}
