
#ifndef OBL_PROGAVA_DTDIRECCION_H
#define OBL_PROGAVA_DTDIRECCION_H

#include <iostream>
#include <string>

class DtDireccion {
  friend std::ostream& operator<<(std::ostream&, DtDireccion&);

 private:
  std::string calle;
  int numeroPuerta;

 public:
  // Constructor por defecto
  DtDireccion();

  // Constructor con parametros
  DtDireccion(const std::string& calle, int numeroPuerta);

  // Getters
  const std::string& getCalle() const;

  int getNumeroPuerta() const;

  // Destructor
  virtual ~DtDireccion();
};

#endif  // OBL_PROGAVA_DTDIRECCION_H
