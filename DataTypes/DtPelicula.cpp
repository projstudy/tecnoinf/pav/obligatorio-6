#include "DtPelicula.h"

DtPelicula::DtPelicula() {
  this->titulo = "";
  this->poster = "";
}

DtPelicula::DtPelicula(const std::string& titulo, const std::string& poster) {
  this->titulo = titulo;
  this->poster = poster;
}

const std::string& DtPelicula::getTitulo() const {
  return titulo;
}

const std::string& DtPelicula::getPoster() const {
  return poster;
}

DtPelicula::~DtPelicula() {}

// Sobrecarga
std::ostream& operator<<(std::ostream& salida, const DtPelicula& pelicula) {
  return salida << "- Titulo: " << pelicula.getTitulo()
                << "\n- Poster: " << pelicula.getPoster() << std::endl;
}
