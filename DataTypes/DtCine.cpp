
#include "DtCine.h"

// Constructor por defecto
DtCine::DtCine() {
  this->id = 0;
  this->direccion = DtDireccion();
}

// Constructor con parametros
DtCine::DtCine(int id, const DtDireccion& direccion) {
  this->id = id;
  this->direccion = direccion;
}

// Getters
int DtCine::getId() const {
  return id;
}

const DtDireccion& DtCine::getDireccion() const {
  return direccion;
}

// Destructor
DtCine::~DtCine() {}

// Sobrecarga

std::ostream& operator<<(std::ostream& salida, const DtCine& cine) {
  auto dt = cine.getDireccion();
  return salida << "- Id: " << cine.getId() << "\n  Direccion: " << dt;
}
