//
//

#include "DtPeliculaExt.h"

DtPeliculaExt::DtPeliculaExt() {
  this->titulo = "";
  this->poster = "";
  this->sinopsis = "";
}

DtPeliculaExt::DtPeliculaExt(const std::string& tituloExt,
                             const std::string& posterExt,
                             const std::string& sinopsisExt) {
  this->sinopsis = sinopsisExt;
  this->titulo = tituloExt;
  this->poster = posterExt;
}

const std::string& DtPeliculaExt::getTitulo() const {
  return titulo;
}

const std::string& DtPeliculaExt::getPoster() const {
  return poster;
}

const std::string& DtPeliculaExt::getSinopsis() const {
  return sinopsis;
}

DtPeliculaExt::~DtPeliculaExt() {}
//
// Sobrecarga

std::ostream& operator<<(std::ostream& salidaExt, DtPeliculaExt& peliculaExt) {
  return salidaExt << "- Titulo: " << peliculaExt.getTitulo()
                   << "\n- Poster: " << peliculaExt.getPoster()
                   << "\n- Sinopsis: " << peliculaExt.getSinopsis();
}
