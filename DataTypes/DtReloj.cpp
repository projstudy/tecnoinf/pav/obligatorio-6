#include "DtReloj.h"
#include <iomanip>

DtReloj::DtReloj() {
  time_t tSac = time(nullptr);  // instante actual
  struct tm* tmP = localtime(&tSac);
  this->anio = tmP->tm_year + 1900;
  this->mes = tmP->tm_mon + 1;
  this->dia = tmP->tm_mday;
  this->hora = tmP->tm_hour;
  this->min = tmP->tm_min;
}

DtReloj::DtReloj(int hora, int min, int dia, int mes, int anio) {
  this->anio = anio;
  this->mes = mes;
  this->dia = dia;
  this->hora = hora;
  this->min = min;
}

int DtReloj::getMin() const {
  return min;
}

DtReloj::~DtReloj() {}

int DtReloj::getHora() const {
  return hora;
}

int DtReloj::getDia() const {
  return dia;
}

int DtReloj::getMes() const {
  return mes;
}

int DtReloj::getAnio() const {
  return anio;
}

bool operator<(const DtReloj& a, const DtReloj& b) {
  if (a.anio != b.anio) {
    return a.anio < b.anio;
  } else if (a.mes != b.mes) {
    return a.mes < b.mes;
  } else if (a.dia != b.dia) {
    return a.dia < b.dia;
  } else if (a.hora != b.hora) {
    return a.hora < b.hora;
  } else if (a.min != b.min) {
    return a.min < b.min;
  } else {
    // son iguales
    return false;
  }
}

// Sobrecarga

std::ostream& operator<<(std::ostream& salida, const DtReloj& reloj) {
  return salida << std::setfill('0') << std::setw(2) << reloj.getDia() << "/"
                << std::setfill('0') << std::setw(2) << reloj.getMes() << "/"
                << reloj.getAnio() << " " << std::setfill('0') << std::setw(2)
                << reloj.getHora() << ":" << std::setfill('0') << std::setw(2)
                << reloj.getMin();
}
