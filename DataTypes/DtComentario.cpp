#include "DtComentario.h"

DtComentario::DtComentario() {
  this->idc = 0;
  this->comentario = "";
  this->nickname = "";
}

DtComentario::DtComentario(int idc,
                           const std::string& comentario,
                           const std::string& nickname,
                           const std::vector<DtComentario>& hijos,
                           int nivel) {
  this->nivel = nivel;
  this->idc = idc;
  this->comentario = comentario;
  this->nickname = nickname;
  this->comentarios = hijos;
}

int DtComentario::getIdc() const {
  return idc;
}

std::vector<DtComentario> DtComentario::getComentarios() const {
  return comentarios;
}

const std::string& DtComentario::getComentario() const {
  return comentario;
}

const std::string& DtComentario::getNickname() const {
  return nickname;
}

int DtComentario::getNivel() const {
  return nivel;
}

DtComentario::~DtComentario() {}

std::ostream& operator<<(std::ostream& salida, const DtComentario& comentario) {
  salida << "[" << comentario.getIdc() << "] " << comentario.getNickname()
         << ": " << comentario.getComentario() << "\n";

  for (auto const& come : comentario.getComentarios()) {
    salida << std::string(come.getNivel() * 4, ' ') << come;
  }
  return salida;
}
