
#ifndef OBL_PROGAVA_DTPELICULA_H
#define OBL_PROGAVA_DTPELICULA_H

#include <iostream>
#include <string>

class DtPelicula {
  friend std::ostream& operator<<(std::ostream&, const DtPelicula&);

 private:
  std::string titulo;
  std::string poster;

 public:
  DtPelicula();

  DtPelicula(const std::string& titulo, const std::string& poster);

  const std::string& getTitulo() const;

  const std::string& getPoster() const;

  virtual ~DtPelicula();
};

#endif  // OBL_PROGAVA_DTPELICULA_H
