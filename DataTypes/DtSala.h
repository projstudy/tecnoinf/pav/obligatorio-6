

#ifndef OBL_PROGAVA_DTSALA_H
#define OBL_PROGAVA_DTSALA_H

#include <vector>
#include "DtFuncion.h"

class DtSala {
  friend std::ostream& operator<<(std::ostream&, const DtSala&);

 private:
  int id;
  int capacidad;
  std::vector<std::pair<DtReloj, DtReloj>> horas;

 public:
  // Copnstrutor por defecto
  DtSala();

  // Constructor con parametros
  DtSala(int id,
         int capacidad,
         const std::vector<std::pair<DtReloj, DtReloj>>&);

  // Getters
  std::vector<std::pair<DtReloj, DtReloj>> getHoras() const;

  int getId() const;

  int getCapacidad() const;

  // Destructor
  virtual ~DtSala();

  // Operacion
};

#endif  // OBL_PROGAVA_DTSALA_H
