//
// Created by mcastro on 04/06/2019.
//

#ifndef OBL_PROGAVA_DTCOMENTARIO_H
#define OBL_PROGAVA_DTCOMENTARIO_H

#include <iostream>
#include <string>
#include <vector>

class DtComentario {
  friend std::ostream& operator<<(std::ostream&, const DtComentario&);

 private:
  int idc;
  std::vector<DtComentario> comentarios;
  std::string comentario;
  std::string nickname;
  int nivel;

 public:
  DtComentario();

  DtComentario(int,
               const std::string& comentario,
               const std::string& nickname,
               const std::vector<DtComentario>&,
               int nivel);

  int getIdc() const;

  std::vector<DtComentario> getComentarios() const;

  const std::string& getComentario() const;

  const std::string& getNickname() const;

  int getNivel() const;

  virtual ~DtComentario();
};

#endif  // OBL_PROGAVA_DTCOMENTARIO_H
