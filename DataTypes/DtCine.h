
#ifndef OBL_PROGAVA_DTCINE_H
#define OBL_PROGAVA_DTCINE_H

#include <string>
#include "DtDireccion.h"

class DtCine {
  friend std::ostream& operator<<(std::ostream&, const DtCine&);

 private:
  int id;
  DtDireccion direccion;

 public:
  // Constructor por defecto
  DtCine();

  // Constructor con parametros
  DtCine(int id, const DtDireccion& direccion);

  // Getters
  int getId() const;

  const DtDireccion& getDireccion() const;

  // Destructor
  virtual ~DtCine();
};

#endif  // OBL_PROGAVA_DTCINE_H
