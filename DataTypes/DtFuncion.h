
#ifndef OBL_PROGAVA_DTFUNCION_H
#define OBL_PROGAVA_DTFUNCION_H

#include <string>
#include "DtReloj.h"

class DtFuncion {
  friend std::ostream& operator<<(std::ostream&, const DtFuncion&);

 private:
  int id;
  int asientosLibres;
  DtReloj horaInicio;
  DtReloj horaFin;

 public:
  // Constructor por defecto
  DtFuncion();

  // Constructor con parametros
  DtFuncion(int id,
            const DtReloj& horaInicio,
            const DtReloj& horaFin,
            int asientos);

  // Getters
  int getAsientosLibres() const;

  int getId() const;

  const DtReloj& getHoraInicio() const;

  const DtReloj& getHoraFin() const;

  // Destructor
  virtual ~DtFuncion();
};

#endif  // OBL_PROGAVA_DTFUNCION_H
