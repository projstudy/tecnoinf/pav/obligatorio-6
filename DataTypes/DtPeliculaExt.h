//
// Created by mcastro on 04/06/2019.
//

#ifndef OBL_PROGAVA_DTPELICULAEXT_H
#define OBL_PROGAVA_DTPELICULAEXT_H

#include <iostream>
#include <string>

class DtPeliculaExt {
  friend std::ostream& operator<<(std::ostream&, DtPeliculaExt&);

 private:
  std::string titulo;
  std::string poster;
  std::string sinopsis;

 public:
  DtPeliculaExt();

  DtPeliculaExt(const std::string& tituloExt,
                const std::string& posterExt,
                const std::string& sinopsisExt);

  const std::string& getTitulo() const;

  const std::string& getPoster() const;

  const std::string& getSinopsis() const;

  virtual ~DtPeliculaExt();
};

#endif  // OBL_PROGAVA_DTPELICULAEXT_H
        // pronto