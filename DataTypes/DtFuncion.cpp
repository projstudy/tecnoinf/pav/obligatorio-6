
#include "DtFuncion.h"

// Constructor por defecto
DtFuncion::DtFuncion() {
  this->id = 0;
  this->horaInicio = DtReloj();
  this->horaFin = DtReloj();
}

// Constructor con parametros
DtFuncion::DtFuncion(int id,
                     const DtReloj& horaInicio,
                     const DtReloj& horaFin,
                     int asientos) {
  this->id = id;
  this->asientosLibres = asientos;
  this->horaInicio = horaInicio;
  this->horaFin = horaFin;
}

// Getters
int DtFuncion::getId() const {
  return id;
}

int DtFuncion::getAsientosLibres() const {
  return asientosLibres;
}

const DtReloj& DtFuncion::getHoraInicio() const {
  return horaInicio;
}

const DtReloj& DtFuncion::getHoraFin() const {
  return horaFin;
}

// Destructor
DtFuncion::~DtFuncion() {}

// Sobrecarga
std::ostream& operator<<(std::ostream& salida, const DtFuncion& funcion) {
  auto dtInicio = funcion.getHoraInicio();
  auto dtFin = funcion.getHoraFin();
  return salida << "- Id: " << funcion.getId()
                << "\n  Cant. asientos disponibles: "
                << funcion.getAsientosLibres() << "\n  Horario: De " << dtInicio
                << " a " << dtFin;
}
