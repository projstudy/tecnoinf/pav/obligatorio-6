//
// Created by Usuario on 9/6/2019.
//

#ifndef OBL_PROGAVA_DTRELOJ_H
#define OBL_PROGAVA_DTRELOJ_H

#include <ctime>
#include <iostream>
#include <string>

class DtReloj {
 public:
  friend std::ostream& operator<<(std::ostream&, const DtReloj&);

  friend bool operator<(const DtReloj&, const DtReloj&);

  DtReloj();

  DtReloj(int hora, int min, int dia, int mes, int anio);

  int getMin() const;

  int getHora() const;

  int getDia() const;

  int getMes() const;

  int getAnio() const;

  virtual ~DtReloj();

 private:
  int min;
  int hora;
  int dia;
  int mes;
  int anio;
};

#endif  // OBL_PROGAVA_DTRELOJ_H
