#include "DtComentarioPuntaje.h"

DtComentarioPuntaje::DtComentarioPuntaje() {
  this->puntajePromedio = 0;
}

DtComentarioPuntaje::DtComentarioPuntaje(
    const std::vector<DtPuntaje>& puntajes,
    const std::vector<DtComentario>& comentarios) {
  this->puntajePromedio = 0;
  for (const auto& puntaje : puntajes) {
    this->puntajePromedio += puntaje.getPuntaje();
  }
  this->puntajePromedio = this->puntajePromedio / puntajes.size();
  this->comentarios = comentarios;
  this->puntajes = puntajes;
}

const std::vector<DtComentario> DtComentarioPuntaje::getComentarios() const {
  return comentarios;
}

const std::vector<DtPuntaje> DtComentarioPuntaje::getPuntajes() const {
  return puntajes;
}

DtComentarioPuntaje::~DtComentarioPuntaje() {}

float DtComentarioPuntaje::getPuntajePromedio() const {
  return puntajePromedio;
}

std::ostream& operator<<(std::ostream& salida, const DtComentarioPuntaje& dt) {
  salida << "\nPuntaje Promedio: " << dt.getPuntajePromedio() << " ("
         << dt.getPuntajes().size() << " usuarios)\n\n";
  salida << "Comentarios: \n";
  auto com = dt.getComentarios();
  for (const auto& co : com) {
    salida << co << std::endl;
  }
  salida << "Puntajes: \n";
  auto puntajes = dt.getPuntajes();
  for (const auto& punt : puntajes) {
    salida << punt;
  }
  return salida;
}
