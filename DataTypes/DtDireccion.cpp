
#include "DtDireccion.h"
#include <stdexcept>

using namespace std;

// Constructor por defecto
DtDireccion::DtDireccion() {
  this->calle = "";
  this->numeroPuerta = 0;
}

// Constructor con parametros
DtDireccion::DtDireccion(const std::string& calle, int numeroPuerta) {
  if (numeroPuerta < 0) {
    throw invalid_argument("numero puerta invalido\n");
  } else {
    this->calle = calle;
    this->numeroPuerta = numeroPuerta;
  }
}

// Getters
const std::string& DtDireccion::getCalle() const {
  return calle;
}

int DtDireccion::getNumeroPuerta() const {
  return numeroPuerta;
}

// Destructor
DtDireccion::~DtDireccion() {}

// Sobrecarga

std::ostream& operator<<(std::ostream& salida, DtDireccion& direccion) {
  return salida << " " << direccion.getCalle() << "\t"
                << direccion.getNumeroPuerta() << "\n";
}
