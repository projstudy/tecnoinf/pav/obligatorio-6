

#ifndef OBL_PROGAVA_DTCOMENTARIOPUNTAJE_H
#define OBL_PROGAVA_DTCOMENTARIOPUNTAJE_H

#include "DtComentario.h"
#include "DtPuntaje.h"
#include "iostream"

class DtComentarioPuntaje {
  friend std::ostream& operator<<(std::ostream&, const DtComentarioPuntaje&);

 private:
  std::vector<DtComentario> comentarios;
  std::vector<DtPuntaje> puntajes;
  float puntajePromedio;

 public:
  DtComentarioPuntaje();

  DtComentarioPuntaje(const std::vector<DtPuntaje>&,
                      const std::vector<DtComentario>&);

  const std::vector<DtComentario> getComentarios() const;

  const std::vector<DtPuntaje> getPuntajes() const;

  virtual ~DtComentarioPuntaje();

  float getPuntajePromedio() const;
};

#endif  // OBL_PROGAVA_DTCOMENTARIOPUNTAJE_H
