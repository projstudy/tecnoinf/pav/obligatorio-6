//
//

#include "DtPuntaje.h"
#include <iostream>

DtPuntaje::DtPuntaje() {
  this->puntaje = 0;
  this->nickname = "";
}

DtPuntaje::DtPuntaje(float puntaje, const std::string& nickname) {
  this->puntaje = puntaje;
  this->nickname = nickname;
}

float DtPuntaje::getPuntaje() const {
  return puntaje;
}

const std::string& DtPuntaje::getNickname() const {
  return nickname;
}

DtPuntaje::~DtPuntaje() {}

std::ostream& operator<<(std::ostream& salida, const DtPuntaje& dt) {
  return salida << dt.getNickname() << ": " << dt.getPuntaje() << std::endl;
}
