
#ifndef OBL_PROGAVA_PELICULA_H
#define OBL_PROGAVA_PELICULA_H

#include <map>
#include <string>
#include <vector>
#include "../DataTypes/DtComentarioPuntaje.h"
#include "../DataTypes/DtPelicula.h"
#include "../DataTypes/DtPeliculaExt.h"
#include "Cine.h"
#include "Comentario.h"
#include "Funcion.h"
#include "Puntaje.h"

class Pelicula {
 private:
  std::string titulo;
  std::string poster;
  std::string sinopsis;
  float puntajePromedio;
  std::vector<Comentario*> comentarios;
  std::vector<Puntaje*> puntajes;
  std::vector<Cine*> cines;
  std::map<int, Funcion*> funciones;

 public:
  // Constructor por defecto
  Pelicula();

  // Constructor con parametros
  Pelicula(const std::string& titulo,
           const std::string& poster,
           const std::string& sinopsis,
           float puntajePromedio);

  // Setters y getters
  const std::string& getTitulo() const;

  void setTitulo(const std::string& titulo);

  const std::string& getPoster() const;

  void setPoster(const std::string& poster);

  const std::string& getSinopsis() const;

  void setSinopsis(const std::string& sinopsis);

  float getPuntajePromedio() const;

  void setPuntajePromedio(float puntajePromedio);

  // Destructor
  virtual ~Pelicula() noexcept(false);

  // Operaciones
  void agregarComentario(Comentario* c);

  void agregarPuntaje(Puntaje* p);

  void agregarCine(Cine* c);

  void agregarFuncion(Funcion* f);

  bool existeFuncion(int);

  // Metodos
  std::vector<DtCine> listarCines();

  bool estaPuntuada();

  float verPuntaje();

  void puntuar(float);

  DtPeliculaExt getDtPeliculaExt();

  DtPelicula getDtPelicula();

  DtComentarioPuntaje getDtComentarioPuntaje();

  std::vector<DtComentario> getDtComentarios();

  void ingresarComentario(const std::string&);

  std::vector<int> eliminarFuncionDePelicula();
};

#endif  // OBL_PROGAVA_PELICULA_H
