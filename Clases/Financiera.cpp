#include "Financiera.h"

Financiera::Financiera() {
  this->descuento = 0;
}
void Financiera::setFinanciera(TipoFinanciera financiera) {
  Financiera::financiera = financiera;
}
void Financiera::setDescuento(float descuento) {
  Financiera::descuento = descuento;
}

Financiera::Financiera(TipoFinanciera financiera, float descuento) {
  this->financiera = financiera;
  this->descuento = descuento;
}

TipoFinanciera Financiera::getFinanciera() const {
  return financiera;
}

float Financiera::getDescuento() const {
  return descuento;
}

Financiera::~Financiera() {}
