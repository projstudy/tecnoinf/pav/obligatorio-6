
#ifndef OBL_PROGAVA_COMENTARIO_H
#define OBL_PROGAVA_COMENTARIO_H

#include <vector>
#include "../DataTypes/DtComentario.h"
#include "Usuario.h"
#include "string"

class Comentario {
 private:
  static int secuenciaId;
  int id;
  std::string comentario;
  Usuario* usuario;
  std::vector<Comentario*> comentarios;

 public:
  int getId() const;

  void setId(int id);

 public:
  // Constructor por defecto
  Comentario();

  // Constructor con parametros
  Comentario(const std::string& comentario, Usuario* usuario);

  // Setters y getters
  const std::string& getComentario() const;

  void setComentario(const std::string& comentario);

  Usuario* getUsuario() const;

  void setUsuario(Usuario* usuario);

  // Destructor
  virtual ~Comentario();

  // Operacion
  void addComentario(Comentario* c);

  // Metodos
  std::vector<DtComentario> getDtComentarios(int nivel = 0);

  DtComentario getDtComentario(int nivel = 0);
};

#endif  // OBL_PROGAVA_COMENTARIO_H
