#include "Comentario.h"

int Comentario::secuenciaId = 0;

// Constructor por defecto
Comentario::Comentario() {
  this->id = ++secuenciaId;
  this->comentario = "";
  this->usuario = new Usuario();
}

// Constructor con parametros
Comentario::Comentario(const std::string& comentario, Usuario* usuario) {
  this->id = ++secuenciaId;
  this->comentario = comentario;
  this->usuario = usuario;
}

// Setters y getters
const std::string& Comentario::getComentario() const {
  return comentario;
}

void Comentario::setComentario(const std::string& comentario) {
  Comentario::comentario = comentario;
}

Usuario* Comentario::getUsuario() const {
  return usuario;
}

void Comentario::setUsuario(Usuario* usuario) {
  Comentario::usuario = usuario;
}

// Destructor
Comentario::~Comentario() {
  for (auto c : comentarios) {
    delete c;
  }
}

// Operacion
void Comentario::addComentario(Comentario* c) {
  this->comentarios.push_back(c);
}

// Metodos
DtComentario Comentario::getDtComentario(int nivel) {
  auto comentariosHijos = this->getDtComentarios(nivel);
  DtComentario dt =
      DtComentario(this->id, this->comentario, this->usuario->getNickname(),
                   comentariosHijos, nivel);
  return dt;
}

std::vector<DtComentario> Comentario::getDtComentarios(int nivel) {
  ++nivel;
  std::vector<DtComentario> coms;
  for (auto& com : comentarios) {
    auto dt = com->getDtComentario(nivel);
    coms.push_back(dt);
  }
  return coms;
}

int Comentario::getId() const {
  return id;
}

void Comentario::setId(int id) {
  Comentario::id = id;
}
