
#include "Usuario.h"
#include "../Manejadores/Sesion.h"

// Constructor por defecto
Usuario::Usuario() {
  this->nickname = "";
  this->imagen = "";
  this->contrasenia = "";
}

// Constructor con parametros
Usuario::Usuario(const std::string& nickname,
                 const std::string& imagen,
                 const std::string& contrasenia) {
  this->nickname = nickname;
  this->imagen = imagen;
  this->contrasenia = contrasenia;
}

// Setters y getters
const std::string& Usuario::getNickname() const {
  return nickname;
}

void Usuario::setNickname(const std::string& nickname) {
  Usuario::nickname = nickname;
}

const std::string& Usuario::getImagen() const {
  return imagen;
}

void Usuario::setImagen(const std::string& imagen) {
  Usuario::imagen = imagen;
}

const std::string& Usuario::getContrasenia() const {
  return contrasenia;
}

void Usuario::setContrasenia(const std::string& contrasenia) {
  Usuario::contrasenia = contrasenia;
}

// Destructor
Usuario::~Usuario() {}

// Metodos
bool Usuario::comprobarContrasenia(const std::string& contrasenia) {
  return this->contrasenia == contrasenia;
}

bool Usuario::esAdmin() const {
  return admin;
}

void Usuario::setAdmin(bool admin) {
  this->admin = admin;
}
