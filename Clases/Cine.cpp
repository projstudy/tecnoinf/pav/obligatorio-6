
#include "Cine.h"

int Cine::secuenciaId = 0;

// Constructor por defecto
Cine::Cine() {
  this->id = ++secuenciaId;
  this->direccion = DtDireccion();
}

// Constructor con parametros
Cine::Cine(const DtDireccion& direccion) {
  this->id = ++secuenciaId;
  this->direccion = direccion;
}

// Setters y getters
int Cine::getId() const {
  return id;
}

void Cine::setId(int id) {
  Cine::id = id;
}

const DtDireccion& Cine::getDireccion() const {
  return direccion;
}

void Cine::setDireccion(const DtDireccion& direccion) {
  Cine::direccion = direccion;
}

// Destructor
Cine::~Cine() {
  for (auto& a : this->salas) {
    delete a.second;
  }
  salas.clear();
}

// Operacion
void Cine::agregarSala(Sala* sala) {
  auto kv = std::make_pair(sala->getId(), sala);
  salas.insert(kv);
}

void Cine::eliminarSala(int id) {
  auto posicionSala = salas.find(id);
  if (posicionSala != salas.end()) {
    this->salas.erase(posicionSala);
  } else {
    throw std::invalid_argument("Sala no encontrada");
  }
}

Sala* Cine::buscarSala(int id) {
  auto posicionSala = salas.find(id);
  if (posicionSala != salas.end()) {
    return posicionSala->second;
  } else {
    throw std::invalid_argument("Sala no encontrada");
  }
}

// Metodos
DtCine Cine::getDtCine() {
  return DtCine(this->id, this->direccion);
}

std::vector<DtFuncion> Cine::getDtFunciones() {
  std::vector<DtFuncion> funcionesDeCine;
  /*recorro las salas del cine */
  for (auto& sala : this->salas) {
    /*me guardo el vector de funciones para cada sala*/
    auto funcionesDeSala = sala.second->getDtFunciones();
    /*recorro las funciones de cada sala*/
    for (auto& x : funcionesDeSala) {
      /*hago un push en el vector de cines*/
      funcionesDeCine.push_back(x);
    }
  }
  return funcionesDeCine;
}

void Cine::eliminarFuncionDeCine(std::vector<int> kPelicula) {
  for (auto& sala : this->salas) {
    try {
      sala.second->eliminarFuncionDeSala(kPelicula);

    } catch (std::invalid_argument& e) {
      throw(e);
    }
  }
}

std::vector<DtSala> Cine::getListaDtSalas() {
  std::vector<DtSala> salida;
  for (auto sala : this->salas) {
    DtSala dtSala = DtSala(sala.second->getId(), sala.second->getCapacidad(),
                           sala.second->getHorasOcupadas());
    salida.push_back(dtSala);
  }
  return salida;
}

DtSala Cine::getDtSala(int idSala) {
  auto kvSala = salas.find(idSala);
  if (kvSala != salas.end()) {
    DtSala dtSala =
        DtSala(kvSala->second->getId(), kvSala->second->getCapacidad(),
               kvSala->second->getHorasOcupadas());
    return dtSala;
  } else {
    throw std::invalid_argument("Sala no encontrada");
  }
}

int Cine::crearFuncion(int idSala,
                       DtReloj horaInicio,
                       DtReloj horaFin,
                       float precio) {
  try {
    auto s = buscarSala(idSala);
    return s->crearFuncion(precio, horaInicio, horaFin);
  } catch (std::invalid_argument& e) {
    throw e;
  }
}
