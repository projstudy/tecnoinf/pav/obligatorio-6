
#ifndef OBL_PROGAVA_USUARIO_H
#define OBL_PROGAVA_USUARIO_H

#include <string>

class Usuario {
 private:
  std::string nickname;
  std::string imagen;
  std::string contrasenia;
  bool admin;

 public:
  // Constructor por defecto
  Usuario();

  // Constructor con parametros
  Usuario(const std::string& nickname,
          const std::string& imagen,
          const std::string& contrasenia);

  // Setters y getters
  bool esAdmin() const;

  void setAdmin(bool);

  const std::string& getNickname() const;

  void setNickname(const std::string& nickname);

  const std::string& getImagen() const;

  void setImagen(const std::string& imagen);

  const std::string& getContrasenia() const;

  void setContrasenia(const std::string& contrasenia);

  // Destructor
  virtual ~Usuario();

  // Metodo
  bool comprobarContrasenia(const std::string&);
};

#endif  // OBL_PROGAVA_USUARIO_H
