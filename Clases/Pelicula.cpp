#include "Pelicula.h"
#include <iostream>
#include "../Manejadores/ManejadorGenerico.h"
#include "../Manejadores/Sesion.h"
// Constructor por defecto
Pelicula::Pelicula() {
  this->titulo = "";
  this->poster = "";
  this->sinopsis = "";
  this->puntajePromedio = 0;
}

// Constructor con parametros
Pelicula::Pelicula(const std::string& titulo,
                   const std::string& poster,
                   const std::string& sinopsis,
                   float puntajePromedio) {
  this->titulo = titulo;
  this->poster = poster;
  this->sinopsis = sinopsis;
  this->puntajePromedio = puntajePromedio;
}

// Setters y getters
const std::string& Pelicula::getTitulo() const {
  return titulo;
}

void Pelicula::setTitulo(const std::string& titulo) {
  Pelicula::titulo = titulo;
}

const std::string& Pelicula::getPoster() const {
  return poster;
}

void Pelicula::setPoster(const std::string& poster) {
  Pelicula::poster = poster;
}

const std::string& Pelicula::getSinopsis() const {
  return sinopsis;
}

void Pelicula::setSinopsis(const std::string& sinopsis) {
  Pelicula::sinopsis = sinopsis;
}

float Pelicula::getPuntajePromedio() const {
  return puntajePromedio;
}

void Pelicula::setPuntajePromedio(float puntajePromedio) {
  Pelicula::puntajePromedio = puntajePromedio;
}

// Destructor
Pelicula::~Pelicula() noexcept(false) {
  auto ids = eliminarFuncionDePelicula();
  for (auto cine : this->cines) {
    try {
      cine->eliminarFuncionDeCine(ids);
    } catch (std::invalid_argument& e) {
      throw e;
    }
  }

  for (auto puntaje : puntajes) {
    delete puntaje;
  }

  for (auto comentario : comentarios) {
    delete comentario;
  }
}

// Operaciones
void Pelicula::agregarComentario(Comentario* comentario) {
  this->comentarios.push_back(comentario);
}

void Pelicula::agregarPuntaje(Puntaje* puntaje) {
  this->puntajes.push_back(puntaje);
}

void Pelicula::agregarCine(Cine* c) {
  bool encontrado = false;
  for (auto i = cines.begin(); i != cines.end() && !encontrado; ++i) {
    if ((*i) == c) {
      encontrado = true;
    }
  }
  if (!encontrado) {
    this->cines.push_back(c);
  }
}

void Pelicula::agregarFuncion(Funcion* funcion) {
  auto kv = std::make_pair(funcion->getId(), funcion);
  this->funciones.insert(kv);
}

bool Pelicula::existeFuncion(int id) {
  auto posicionFuncion = funciones.find(id);
  return posicionFuncion != funciones.end();
}

// Metodos
std::vector<DtCine> Pelicula::listarCines() {
  std::vector<DtCine> cines;
  for (auto& cine : this->cines) {
    cines.push_back(cine->getDtCine());
  }
  return cines;
}

bool Pelicula::estaPuntuada() {
  auto manejadorSesion = ManejadorSesion::getInstancia();
  auto usuario = manejadorSesion->getSesionUsuario();
  bool puntuada = false;
  auto it = puntajes.begin();
  while (it != puntajes.end() && !puntuada) {
    auto puntaje = *it;
    if (usuario == puntaje->getUsuario()) {
      puntuada = true;
    }
    it++;
  }
  return puntuada;
}

float Pelicula::verPuntaje() {
  auto manejadorSesion = ManejadorSesion::getInstancia();
  auto usuario = manejadorSesion->getSesionUsuario();
  float valorPuntaje = 0;
  bool noEncontrada = true;
  auto it = puntajes.begin();
  while (it != puntajes.end() && noEncontrada) {
    auto puntaje = *it;
    if (usuario == puntaje->getUsuario()) {
      valorPuntaje = puntaje->getPuntaje();
      noEncontrada = false;
    }
    it++;
  }
  return valorPuntaje;
}

void Pelicula::puntuar(float valorPuntaje) {
  auto manejadorSesion = ManejadorSesion::getInstancia();
  auto usuario = manejadorSesion->getSesionUsuario();
  bool sePuntuo = false;
  auto it = puntajes.begin();
  while (it != puntajes.end() && !sePuntuo) {
    auto puntaje = *it;
    if (usuario == puntaje->getUsuario()) {
      sePuntuo = true;
      puntaje->setPuntaje(valorPuntaje);
    }
    it++;
  }
  if (!sePuntuo) {
    auto puntaje = new Puntaje(valorPuntaje, usuario);
    this->agregarPuntaje(puntaje);
  }
}

DtPeliculaExt Pelicula::getDtPeliculaExt() {
  return DtPeliculaExt(this->titulo, this->poster, this->sinopsis);
}

DtPelicula Pelicula::getDtPelicula() {
  return DtPelicula(this->titulo, this->poster);
}

DtComentarioPuntaje Pelicula::getDtComentarioPuntaje() {
  std::vector<DtComentario> vecComentarios;
  std::vector<DtPuntaje> vecPuntajes;
  for (auto& comentario : this->comentarios) {
    auto dt = comentario->getDtComentario();
    vecComentarios.push_back(dt);
  }
  for (auto& puntaje : this->puntajes) {
    auto dt = puntaje->getDtPuntaje();
    vecPuntajes.push_back(dt);
  }
  return DtComentarioPuntaje(vecPuntajes, vecComentarios);
}

std::vector<DtComentario> Pelicula::getDtComentarios() {
  std::vector<DtComentario> comentarios;
  for (auto& comentario : this->comentarios) {
    DtComentario dt = comentario->getDtComentario();
    comentarios.push_back(dt);
  }
  return comentarios;
}

void Pelicula::ingresarComentario(const std::string& texto) {
  auto manejadorSesion = ManejadorSesion::getInstancia();
  auto Manejador = Manejador::getInstancia();
  auto usuario = manejadorSesion->getSesionUsuario();
  auto comentario = new Comentario(texto, usuario);
  Manejador->agregarComentario(comentario);
  agregarComentario(comentario);
}

std::vector<int> Pelicula::eliminarFuncionDePelicula() {
  std::vector<int> ids;
  for (auto a : funciones) {
    ids.push_back(a.second->getId());
  }
  funciones.clear();
  return ids;
}
