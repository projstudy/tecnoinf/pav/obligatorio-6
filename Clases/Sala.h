
#ifndef OBL_PROGAVA_SALA_H
#define OBL_PROGAVA_SALA_H

#include <map>
#include <vector>
#include "../DataTypes/DtFuncion.h"
#include "Funcion.h"

class Sala {
 private:
  int id;
  int capacidad;
  std::map<int, Funcion*> funciones;
  static int secuenciaId;

 public:
  // Constructor por defecto
  Sala();

  // Constructor con parametros
  Sala(int capacidad);

  // Setters y getters
  int getId() const;

  void setId(int id);

  int getCapacidad() const;

  void setCapacidad(int capacidad);

  // Destructor
  virtual ~Sala();

  // Operaciones
  void agregarFuncion(Funcion* f);

  bool existeFuncion(int id);

  void eliminarFuncion(int id);

  // Metodos
  std::vector<DtFuncion> getDtFunciones();

  void eliminarFuncionDeSala(std::vector<int>);

  std::vector<std::pair<DtReloj, DtReloj>> getHorasOcupadas();

  int crearFuncion(float, DtReloj, DtReloj);

  Sala* buscarSala(int id);
};

#endif  // OBL_PROGAVA_SALA_H
