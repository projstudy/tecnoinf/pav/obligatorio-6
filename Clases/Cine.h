
#ifndef OBL_PROGAVA_CINE_H
#define OBL_PROGAVA_CINE_H

#include <string>
#include <vector>
#include "../DataTypes/DtCine.h"
#include "../DataTypes/DtFuncion.h"
#include "../DataTypes/DtSala.h"
#include "Sala.h"

class Cine {
 private:
  static int secuenciaId;
  int id;
  DtDireccion direccion;
  std::map<int, Sala*> salas;

 public:
  // Constructor por defecto
  Cine();

  // Constructor con parametros
  Cine(const DtDireccion& direccion);

  // Setters y getters
  int getId() const;

  void setId(int id);

  const DtDireccion& getDireccion() const;

  void setDireccion(const DtDireccion& direccion);

  // Destructor
  virtual ~Cine();

  // Operaciones
  void agregarSala(Sala* s);

  void eliminarSala(int id);

  Sala* buscarSala(int id);

  // Metodos
  DtCine getDtCine();

  std::vector<DtFuncion> getDtFunciones();

  void eliminarFuncionDeCine(std::vector<int>);

  std::vector<DtSala> getListaDtSalas();

  DtSala getDtSala(int idSala);

  int crearFuncion(int, DtReloj, DtReloj, float precio);
};

#endif  // OBL_PROGAVA_CINE_H
