
#ifndef OBL_PROGAVA_CREDITO_H
#define OBL_PROGAVA_CREDITO_H

#include <string>
#include "Financiera.h"
#include "Reserva.h"

class Credito : public Reserva {
 private:
  Financiera financiera;

 public:
  // Constructor por defecto
  Credito();

  // Constructor con parametros
  Credito(int asiento,
          float costoTotal,
          Usuario* usuario,
          const Financiera& financiera);

  // Setters y getters
  const Financiera& getFinanciera() const;

  void setFinanciera(const Financiera& financiera);

  // Destructor
  virtual ~Credito();
};

#endif  // OBL_PROGAVA_CREDITO_H
