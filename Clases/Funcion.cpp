#include "Funcion.h"

int Funcion::secuenciaId = 0;
// Constructor por defecto
Funcion::Funcion() {
  this->precio = 0;
  this->id = ++secuenciaId;
  this->horaInicio = DtReloj();
  this->horaFin = DtReloj();
}

// Constructor con parametros
Funcion::Funcion(float precio,
                 const DtReloj& horaInicio,
                 const DtReloj& horaFin) {
  this->precio = precio;
  this->id = ++secuenciaId;
  this->horaInicio = horaInicio;
  this->horaFin = horaFin;
}

// Setters y getters
float Funcion::getPrecio() const {
  return precio;
}

void Funcion::setPrecio(float precio) {
  this->precio = precio;
}

int Funcion::getId() const {
  return id;
}

void Funcion::setId(int id) {
  Funcion::id = id;
}

const DtReloj& Funcion::getHoraInicio() const {
  return horaInicio;
}

void Funcion::setHoraInicio(const DtReloj& horaInicio) {
  Funcion::horaInicio = horaInicio;
}

const DtReloj& Funcion::getHoraFin() const {
  return horaFin;
}

void Funcion::setHoraFin(const DtReloj& horaFin) {
  Funcion::horaFin = horaFin;
}

// Destructor
Funcion::~Funcion() {
  for (auto r : reservas) {
    delete r;
  }
}

// Operacion
void Funcion::addReserva(Reserva* s) {
  this->reservas.push_back(s);
}

// Metodos
int Funcion::getCantidadReservadas() {
  int reservados = 0;
  for (const auto reserva : reservas) {
    reservados += reserva->getAsiento();
  }
  return reservados;
}
