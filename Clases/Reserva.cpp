#include "Reserva.h"

// Constructor por defecto
Reserva::Reserva() {
  this->asiento = 0;
  this->costoTotal = 0;
  this->usuario = new Usuario();
}

// Constructor con parametros
Reserva::Reserva(int asiento, float costoTotal, Usuario* usuario) {
  this->asiento = asiento;
  this->costoTotal = costoTotal;
  this->usuario = usuario;
}

// Setters y getters
int Reserva::getAsiento() const {
  return asiento;
}

void Reserva::setAsiento(int asiento) {
  Reserva::asiento = asiento;
}

float Reserva::getCostoTotal() const {
  return costoTotal;
}

void Reserva::setCostoTotal(float costoTotal) {
  Reserva::costoTotal = costoTotal;
}

Usuario* Reserva::getUsuario() const {
  return usuario;
}

void Reserva::setUsuairo(Usuario* usuario) {
  Reserva::usuario = usuario;
}

// Destructor
Reserva::~Reserva() {}
