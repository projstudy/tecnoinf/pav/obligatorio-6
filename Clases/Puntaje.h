
#ifndef OBL_PROGAVA_PUNTAJE_H
#define OBL_PROGAVA_PUNTAJE_H

#include <vector>
#include "../DataTypes/DtPuntaje.h"
#include "Usuario.h"

class Puntaje {
 private:
  float puntaje;
  Usuario* usuario;

 public:
  // Constructor por defecto
  Puntaje();

  // Constructor con parametros
  Puntaje(float puntaje, Usuario* usuario);

  // Setters y getters
  float getPuntaje() const;

  void setPuntaje(float puntaje);

  Usuario* getUsuario() const;

  void setUsuario(Usuario* usuario);

  // Destructor
  virtual ~Puntaje();

  // Metodos
  bool estaPuntuada();

  float verPuntaje();

  void puntuar(float);

  DtPuntaje getDtPuntaje();
};

#endif  // OBL_PROGAVA_PUNTAJE_H
