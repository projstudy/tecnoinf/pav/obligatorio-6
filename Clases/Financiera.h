#ifndef OBL_PROGAVA_FINANCIERA_H
#define OBL_PROGAVA_FINANCIERA_H

#include "../Tipos/TipoFinanciera.h"

class Financiera {
 private:
  TipoFinanciera financiera;
  float descuento;

 public:
  Financiera();

  Financiera(TipoFinanciera financiera, float descuento);

  TipoFinanciera getFinanciera() const;

  float getDescuento() const;

  virtual ~Financiera();
  void setFinanciera(TipoFinanciera financiera);
  void setDescuento(float descuento);
};

#endif  // OBL_PROGAVA_FINANCIERA_H
