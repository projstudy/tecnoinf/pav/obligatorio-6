#ifndef OBL_PROGAVA_FUNCION_H
#define OBL_PROGAVA_FUNCION_H

#include "../DataTypes/DtFuncion.h"
#include "../DataTypes/DtReloj.h"
#include "../DataTypes/DtSala.h"
#include "Reserva.h"

#include <string>
#include <vector>

class Funcion {
 private:
  static int secuenciaId;
  float precio;
  int id;
  DtReloj horaInicio;
  DtReloj horaFin;
  std::vector<Reserva*> reservas;

 public:
  // Constructor por defecto
  Funcion();

  // Constructor con parametros
  Funcion(float precio, const DtReloj& horaInicio, const DtReloj& horaFin);

  // Setters y getters
  float getPrecio() const;

  void setPrecio(float precio);

  int getId() const;

  void setId(int id);

  const DtReloj& getHoraInicio() const;

  void setHoraInicio(const DtReloj& hora);

  const DtReloj& getHoraFin() const;

  void setHoraFin(const DtReloj& hora);

  // Destructor
  virtual ~Funcion();

  // Operacion
  void addReserva(Reserva* s);

  // Metodos
  int getCantidadReservadas();

  bool eliminarPeliculaDeFuncion(std::string);
};

#endif  // OBL_PROGAVA_FUNCION_H
