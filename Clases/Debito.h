
#ifndef OBL_PROGAVA_DEBITO_H
#define OBL_PROGAVA_DEBITO_H

#include <string>
#include "Reserva.h"

class Debito : public Reserva {
 private:
  std::string banco;

 public:
  // Constructor por defecto
  Debito();

  // Constructor con parametros
  Debito(int asiento,
         int costoTotal,
         Usuario* usuario,
         const std::string& banco);

  // Setters y getters
  const std::string& getBanco() const;

  void setBanco(const std::string& banco);

  // Destructor
  virtual ~Debito();
};

#endif  // OBL_PROGAVA_DEBITO_H
