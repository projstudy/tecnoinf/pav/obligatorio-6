
#include "Sala.h"
#include "../Manejadores/ManejadorGenerico.h"
int Sala::secuenciaId = 0;

// Constructor por defecto
Sala::Sala() {
  this->id = ++secuenciaId;
  this->capacidad = 0;
}

// Constructor con parametros
Sala::Sala(int capacidad) {
  this->id = ++secuenciaId;
  this->capacidad = capacidad;
}

// Settersy getters
int Sala::getId() const {
  return id;
}

void Sala::setId(int id) {
  Sala::id = id;
}

int Sala::getCapacidad() const {
  return capacidad;
}

void Sala::setCapacidad(int capacidad) {
  Sala::capacidad = capacidad;
}

// Destructor
Sala::~Sala() {
  for (auto& a : this->funciones) {
    delete a.second;
  }
  funciones.clear();
}

// Operaciones
void Sala::agregarFuncion(Funcion* f) {
  auto kv = std::pair<int, Funcion*>(f->getId(), f);
  this->funciones.insert(kv);
}

bool Sala::existeFuncion(int id) {
  auto posicionFuncion = funciones.find(id);
  return posicionFuncion != funciones.end();
}

void Sala::eliminarFuncion(int id) {
  auto posicionFuncion = funciones.find(id);
  if (posicionFuncion != funciones.end()) {
    auto funcion = posicionFuncion->second;
    this->funciones.erase(posicionFuncion);
    delete funcion;
  } else {
    throw std::invalid_argument("Funcion no encontrada");
  }
}

// Metodos
std::vector<DtFuncion> Sala::getDtFunciones() {
  std::vector<DtFuncion> funciones;
  for (auto& kv : this->funciones) {
    int ocupadas = kv.second->getCantidadReservadas();
    DtFuncion dt = DtFuncion(kv.second->getId(), kv.second->getHoraInicio(),
                             kv.second->getHoraFin(), capacidad - ocupadas);
    funciones.push_back(dt);
  }
  return funciones;
}

std::vector<std::pair<DtReloj, DtReloj>> Sala::getHorasOcupadas() {
  std::vector<std::pair<DtReloj, DtReloj>> horas;
  for (auto funcion : funciones) {
    // DtFuncion dt =
    /*  DtFuncion(manejadorSesion.second->getId(),
       manejadorSesion.second->getHoraInicio(),
                manejadorSesion.second->getHoraFin(), this->capacidad -
       ocupadas);*/
    auto parHora = std::pair<DtReloj, DtReloj>(funcion.second->getHoraInicio(),
                                               funcion.second->getHoraFin());
    horas.push_back(parHora);
  }
  return horas;
}

void Sala::eliminarFuncionDeSala(std::vector<int> idFunciones) {
  for (auto id : idFunciones) {
    if (existeFuncion(id)) {
      try {
        eliminarFuncion(id);
      } catch (std::invalid_argument& e) {
        throw e;
      }
    }
  }
}

int Sala::crearFuncion(float precio, DtReloj horaInicio, DtReloj horaFin) {
  Funcion* funcion = new Funcion(precio, horaInicio, horaFin);
  Manejador* manejador = Manejador::getInstancia();
  manejador->agregarFuncion(funcion);
  auto par = std::pair<int, Funcion*>(funcion->getId(), funcion);
  this->funciones.insert(par);
  return funcion->getId();
}
