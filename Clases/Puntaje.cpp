
#include "Puntaje.h"
#include "../Manejadores/Sesion.h"

Puntaje::Puntaje() {
  this->puntaje = 0;
  this->usuario = new Usuario();
}

// Constructor con parametros
Puntaje::Puntaje(float puntaje, Usuario* usuario) {
  this->puntaje = puntaje;
  this->usuario = usuario;
}

// Setters y getters
float Puntaje::getPuntaje() const {
  return puntaje;
}

void Puntaje::setPuntaje(float puntaje) {
  Puntaje::puntaje = puntaje;
}

Usuario* Puntaje::getUsuario() const {
  return usuario;
}

void Puntaje::setUsuario(Usuario* usuario) {
  Puntaje::usuario = usuario;
}

// Destructor
Puntaje::~Puntaje() {}

// Metodos

void Puntaje::puntuar(float puntaje) {
  this->puntaje = puntaje;
}

DtPuntaje Puntaje::getDtPuntaje() {
  return DtPuntaje(this->puntaje, this->usuario->getNickname());
}
