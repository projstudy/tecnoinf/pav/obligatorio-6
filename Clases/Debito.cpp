#include "Debito.h"

// Constructor por defecto
Debito::Debito() : Reserva() {
  this->banco = "";
}

// Constructor con parametros
Debito::Debito(int asiento,
               int costoTotal,
               Usuario* usuario,
               const std::string& banco)
    : Reserva(asiento, costoTotal, usuario) {
  this->banco = banco;
}

// Setters y getters
const std::string& Debito::getBanco() const {
  return banco;
}

void Debito::setBanco(const std::string& banco) {
  Debito::banco = banco;
}

// Destructor
Debito::~Debito() {}
