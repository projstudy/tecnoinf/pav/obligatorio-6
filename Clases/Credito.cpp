
#include "Credito.h"

// Constructor por defecto
Credito::Credito() : Reserva() {}

Credito::Credito(int asiento,
                 float costoTotal,
                 Usuario* usuario,
                 const Financiera& financiera)
    : Reserva(asiento, costoTotal, usuario) {
  this->financiera = financiera;
}

// Setters y getters
const Financiera& Credito::getFinanciera() const {
  return financiera;
}

void Credito::setFinanciera(const Financiera& financiera) {
  Credito::financiera = financiera;
}

// Destructor
Credito::~Credito() {}
