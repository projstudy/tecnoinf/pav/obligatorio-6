
#ifndef OBL_PROGAVA_RESERVA_H
#define OBL_PROGAVA_RESERVA_H

#include "Usuario.h"

class Reserva {
 private:
  int asiento;
  float costoTotal;
  Usuario* usuario;

 public:
  // Constructor por defecto
  Reserva();

  // Constructor con parametros
  Reserva(int asiento, float costoTotal, Usuario* usuario);

  // Setters y getters
  int getAsiento() const;

  void setAsiento(int asiento);

  float getCostoTotal() const;

  void setCostoTotal(float costoTotal);

  Usuario* getUsuario() const;

  void setUsuairo(Usuario* usuario);

  // Destructor
  virtual ~Reserva() = 0;
};

#endif  // OBL_PROGAVA_RESERVA_H
