#include "Reloj.h"

Reloj* Reloj::instancia = nullptr;

Reloj* Reloj::getInstancia() {
  if (instancia == nullptr) {
    instancia = new Reloj();
  }
  return instancia;
}

DtReloj Reloj::getReloj() {
  return this->reloj;
}

void Reloj::setReloj(DtReloj reloj) {
  this->reloj = reloj;
}

Reloj::Reloj() {}

Reloj::~Reloj() {}
