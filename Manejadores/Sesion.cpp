//
//

#include "Sesion.h"

ManejadorSesion* ManejadorSesion::instancia = nullptr;

ManejadorSesion* ManejadorSesion::getInstancia() {
  if (instancia == nullptr) {
    instancia = new ManejadorSesion();
  }
  return instancia;
}

Usuario* ManejadorSesion::getSesionUsuario() {
  return this->sesionUsuario;
}

void ManejadorSesion::setSesionUsuario(Usuario* usuario) {
  this->sesionUsuario = usuario;
}

ManejadorSesion::ManejadorSesion() {
  this->sesionUsuario = nullptr;
}

ManejadorSesion::~ManejadorSesion() {}