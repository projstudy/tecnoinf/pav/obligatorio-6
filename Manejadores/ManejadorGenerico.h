//
// Created by e974616 on 6/28/2019.
//

#ifndef OBL_PROGAVA_MANEJADORGENERICO_H
#define OBL_PROGAVA_MANEJADORGENERICO_H

#include "../Clases/Cine.h"
#include "../Clases/Comentario.h"
#include "../Clases/Financiera.h"
#include "../Clases/Funcion.h"
#include "../Clases/Pelicula.h"

class Manejador {
 private:
  Manejador();
  static Manejador* instancia;
  std::map<int, Cine*> cines;
  std::map<int, Comentario*> comentarios;
  std::map<int, Financiera> financieras;
  std::map<int, Funcion*> funciones;
  std::map<std::string, Pelicula*> peliculas;
  std::map<std::string, Usuario*> usuarios;

 public:
  static Manejador* getInstancia();

  std::vector<Usuario*> getUsuarios();
  Usuario* buscarUsuario(const std::string&);
  void agregarMUsuario(Usuario*);
  bool existeUsuario(const std::string&);
  void eliminarUsuario(const std::string&);

  std::vector<Pelicula*> getPelicula();
  Pelicula* buscarPelicula(const std::string&);
  void agregarPelicula(Pelicula*);
  bool existePelicula(const std::string&);
  void eliminarPelicula(const std::string&);

  std::vector<Funcion*> getFuncion();
  Funcion* buscarFuncion(const int&);
  void agregarFuncion(Funcion*);
  bool existeFuncion(const int&);
  void eliminarFuncion(const int&);

  std::vector<Financiera> getFinanciera();
  Financiera buscarFinanciera(TipoFinanciera);
  void agregarFinanciera(Financiera);
  bool existeFinanciera(TipoFinanciera);
  void eliminarFinanciera(TipoFinanciera);

  std::vector<Comentario*> getComentarios();
  Comentario* buscarComentario(int);
  void agregarComentario(Comentario*);
  bool existeComentario(int);
  void eliminarComentario(int);

  std::vector<Cine*> getCines();
  Cine* buscarCine(int);
  void agregarCine(Cine*);
  bool existeCine(int);
  void eliminarCine(int);

  virtual ~Manejador();
};

#endif  // OBL_PROGAVA_MANEJADORGENERICO_H
