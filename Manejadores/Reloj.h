

#ifndef OBL_PROGAVA_RELOJ_H
#define OBL_PROGAVA_RELOJ_H

#include "../DataTypes/DtReloj.h"
class Reloj {
 private:
  static Reloj* instancia;
  DtReloj reloj;
  Reloj();

 public:
  static Reloj* getInstancia();
  void setReloj(DtReloj);
  DtReloj getReloj();
  virtual ~Reloj();
};

#endif  // OBL_PROGAVA_RELOJ_H
