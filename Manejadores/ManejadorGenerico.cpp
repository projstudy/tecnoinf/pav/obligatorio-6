//
// Created by e974616 on 6/28/2019.
//

#include "ManejadorGenerico.h"
using namespace std;

Manejador* Manejador::instancia = nullptr;

Manejador* Manejador::getInstancia() {
  if (instancia == nullptr)
    instancia = new Manejador();
  return instancia;
}

Manejador::~Manejador() = default;

Manejador::Manejador() = default;

std::vector<Cine*> Manejador::getCines() {
  vector<Cine*> lstCines;
  for (const auto& kv : cines) {
    lstCines.push_back(kv.second);
  }
  return lstCines;
}

Cine* Manejador::buscarCine(int id) {
  auto posicionCine = cines.find(id);
  if (posicionCine != cines.end()) {
    return posicionCine->second;
  } else {
    throw invalid_argument("Cine no encontrado");
  }
}

void Manejador::agregarCine(Cine* cine) {
  auto kv = std::pair<int, Cine*>(cine->getId(), cine);
  cines.insert(kv);
}

bool Manejador::existeCine(int id) {
  auto posicionCine = cines.find(id);
  return posicionCine != cines.end();
}

void Manejador::eliminarCine(int id) {
  auto posicionCine = cines.find(id);
  if (posicionCine != cines.end()) {
    this->cines.erase(posicionCine);
  } else {
    throw invalid_argument("Cine no encontrado");
  }
}

std::vector<Comentario*> Manejador::getComentarios() {
  vector<Comentario*> lstComentario;
  for (const auto& kv : comentarios) {
    lstComentario.push_back(kv.second);
  }
  return lstComentario;
}

Comentario* Manejador::buscarComentario(int id) {
  auto posicionComentario = comentarios.find(id);
  if (posicionComentario != comentarios.end()) {
    return posicionComentario->second;
  } else {
    throw std::invalid_argument("Comentario no encontrado");
  }
}

void Manejador::agregarComentario(Comentario* comentario) {
  auto kv = std::pair<int, Comentario*>(comentario->getId(), comentario);
  comentarios.insert(kv);
}

bool Manejador::existeComentario(int id) {
  auto posicionComentario = comentarios.find(id);
  return posicionComentario != comentarios.end();
}

void Manejador::eliminarComentario(int id) {
  auto posicionComentario = comentarios.find(id);
  if (posicionComentario != comentarios.end()) {
    this->comentarios.erase(posicionComentario);
  } else {
    throw invalid_argument("Comentario no encontrado");
  }
}

vector<Financiera> Manejador::getFinanciera() {
  vector<Financiera> lstFinanciera;
  for (const auto& kv : financieras) {
    lstFinanciera.push_back(kv.second);
  }
  return lstFinanciera;
}

Financiera Manejador::buscarFinanciera(TipoFinanciera financiera) {
  auto posicionFinanciera = financieras.find(financiera);
  if (posicionFinanciera != financieras.end()) {
    return posicionFinanciera->second;
  } else {
    throw invalid_argument("Financiera no encontrada");
  }
}

void Manejador::agregarFinanciera(Financiera financiera) {
  auto kv = std::pair<TipoFinanciera, Financiera>(financiera.getFinanciera(),
                                                  financiera);
  financieras.insert(kv);
}

bool Manejador::existeFinanciera(TipoFinanciera financiera) {
  auto posicionFinanciera = financieras.find(financiera);
  return posicionFinanciera != financieras.end();
}

void Manejador::eliminarFinanciera(TipoFinanciera financiera) {
  auto posicionFinanciera = financieras.find(financiera);
  if (posicionFinanciera != financieras.end()) {
    this->financieras.erase(posicionFinanciera);
  } else {
    throw invalid_argument("Financiera no encontrada");
  }
}

std::vector<Funcion*> Manejador::getFuncion() {
  vector<Funcion*> lstFunciones;
  for (const auto& kv : funciones) {
    lstFunciones.push_back(kv.second);
  }
  return lstFunciones;
}

Funcion* Manejador::buscarFuncion(const int& id) {
  auto posicionFuncion = funciones.find(id);
  if (posicionFuncion != funciones.end()) {
    return posicionFuncion->second;
  } else {
    throw invalid_argument("Funcion no encontrada");
  }
}

void Manejador::agregarFuncion(Funcion* funcion) {
  auto kv = std::pair<int, Funcion*>(funcion->getId(), funcion);
  funciones.insert(kv);
}

bool Manejador::existeFuncion(const int& id) {
  auto posicionFunciones = funciones.find(id);
  return posicionFunciones != funciones.end();
}

void Manejador::eliminarFuncion(const int& id) {
  auto posicionFuncion = funciones.find(id);
  if (posicionFuncion != funciones.end()) {
    this->funciones.erase(posicionFuncion);
  } else {
    throw invalid_argument("Usuario no encontrado");
  }
}

std::vector<Pelicula*> Manejador::getPelicula() {
  vector<Pelicula*> lstPelicula;
  for (const auto& kv : peliculas) {
    lstPelicula.push_back(kv.second);
  }
  return lstPelicula;
}

Pelicula* Manejador::buscarPelicula(const std::string& titulo) {
  auto posicionPelicula = peliculas.find(titulo);
  if (posicionPelicula != peliculas.end()) {
    return posicionPelicula->second;
  } else {
    throw invalid_argument("Pelicula no encontrada");
  }
}

void Manejador::agregarPelicula(Pelicula* pelicula) {
  auto kv = std::pair<string, Pelicula*>(pelicula->getTitulo(), pelicula);
  peliculas.insert(kv);
}

bool Manejador::existePelicula(const std::string& titulo) {
  auto posicionPelicula = peliculas.find(titulo);
  return posicionPelicula != peliculas.end();
}

void Manejador::eliminarPelicula(const std::string& titulo) {
  auto posicionPelicula = peliculas.find(titulo);
  if (posicionPelicula != peliculas.end()) {
    this->peliculas.erase(posicionPelicula->first);
  } else {
    throw invalid_argument("Pelicula no encontrada");
  }
}

std::vector<Usuario*> Manejador::getUsuarios() {
  vector<Usuario*> lstUsuarios;
  for (const auto& kv : usuarios) {
    lstUsuarios.push_back(kv.second);
  }
  return lstUsuarios;
}

Usuario* Manejador::buscarUsuario(const std::string& nickname) {
  auto posicionUsuario = usuarios.find(nickname);
  if (posicionUsuario != usuarios.end()) {
    return posicionUsuario->second;
  } else {
    throw invalid_argument("Usuario no encontrado");
  }
}

void Manejador::agregarMUsuario(Usuario* usuario) {
  auto kv = std::pair<string, Usuario*>(usuario->getNickname(), usuario);
  usuarios.insert(kv);
}

bool Manejador::existeUsuario(const std::string& nickname) {
  auto posicionUsuario = usuarios.find(nickname);
  return posicionUsuario != usuarios.end();
}

void Manejador::eliminarUsuario(const std::string& nickname) {
  auto posicionUsuario = usuarios.find(nickname);
  if (posicionUsuario != usuarios.end()) {
    this->usuarios.erase(posicionUsuario);
  } else {
    throw invalid_argument("Usuario no encontrado");
  }
}