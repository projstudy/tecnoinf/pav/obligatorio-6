//
// Created by Usuario on 6/6/2019.
//

#ifndef OBL_PROGAVA_SESION_H
#define OBL_PROGAVA_SESION_H

#include "../Clases/Usuario.h"

class ManejadorSesion {
 private:
  static ManejadorSesion* instancia;
  Usuario* sesionUsuario;
  ManejadorSesion();

 public:
  static ManejadorSesion* getInstancia();
  Usuario* getSesionUsuario();
  void setSesionUsuario(Usuario*);

  virtual ~ManejadorSesion();
};

#endif  // OBL_PROGAVA_SESION_H
