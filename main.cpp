/**
 * Tecnologo Nocturno Latu - 2019
 *
 * Franco Rodriguez  4.684.572-7
 * Juan Mussini      4.442.755-3
 * Mathias Castro    4.523.221-8
 * Mariano Zunino    4.974.616-4
 *
 * */

#include <iostream>
#include "UI/Menu.h"

int main() {
  Menu m = Menu();
  m.menuPrincipal();
  return 0;
}
