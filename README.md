Obligatorio 1 PAV.
---

Usar el IDE (o no) que se les cante, pero **no agregar** las "carpetas de los ides"
 (.idea, cmake-build, etc) proyectos al commit.

Respetar la estructura de las carpetitas: **Clases**, **DataTypes** y **Tipos**

Como usar con CLion
---

1- Clonar el repositorio: git clone https://gitlab.com/mzunino/obl-progava

2- Ir a la pantalla de bienvenida de CLion y elegir la opción Import Project from Sources:

![clion_import](http://i.imgur.com/yYqlZVv.png)

3- Seleccionar el directorio del repositorio recien clonado (en este caso obl-progava), y presionar OK:


4- Seleccionar los archivos de codigo fuente que pertenecen al proyecto (.cpp y .h) y presionar OK


---

- Es clave siempre realizar un `git pull` antes de realizar un `commit`, de esta forma se evitan complicaciones con los 
conflictos en `merge`.

- Evitar que un commit tenga mas de un cambio a la vez:
     
     O sea, por ejemplo si estoy implementando la funcionalidad "Agregar Usuarios", lo ideal es que el commit solo tenga
     cambios referentes a "Agregar Usuario" y no cambios sin relacion con el commit (ej: Fix de scanf en main).
    
- Ante la duda, la mas tetuda. 

