#Colores y otros chiches
COM_COLOR   = \033[0;34m
OBJ_COLOR   = \033[0;36m
OK_COLOR    = \033[0;32m
ERROR_COLOR = \033[0;31m
WARN_COLOR  = \033[0;33m
NO_COLOR    = \033[m
OK_STRING    = "[OK]"
ERROR_STRING = "[ERROR]"
WARN_STRING  = "[WARNING]"
COM_STRING   = "Compiling"
LD_STRING    = "Linking"
CL_STRING    = "Cleaning"

#flags
CC = g++
CFLAGS =-O3 -Wall -Wextra -Wpedantic -Werror -MMD
BUILD = build
#nombre del binario
BIN = obl

VPATH := Clases DataTypes Tipos Controladores Manejadores UI
SRCS := $(wildcard Clases/*.cpp DataTypes/*.cpp Controladores/*.cpp UI/*.cpp Manejadores/*.cpp *.cpp)
OBJS := $(patsubst %.cpp,%.o,$(addprefix $(BUILD)/,$(notdir $(SRCS))))
DEPS := $(OBJS:.o=.d)

$(shell mkdir -p $(BUILD))

all: $(OBJS)
	@$(CC) -o $(BUILD)/$(BIN) $^ 2> $@.log;\
		RESULT=$$?; \
        if [ $$RESULT -ne 0 ]; then \
            printf "%-60b%b" "$(WARN_COLOR)Binario $(OK_COLOR)$(BUILD)/$(BIN)" "$(ERROR_COLOR)$(ERROR_STRING)$(NO_COLOR)\n"; \
        elif [ -s $@.log ]; then \
            printf "%-60b%b" "$(WARN_COLOR)Binario $(OK_COLOR)$(BUILD)/$(BIN)" "$(WARN_COLOR)$(WARN_STRING)$(NO_COLOR)\n"; \
        else  \
            printf "%-60b%b" "$(WARN_COLOR)Binario $(OK_COLOR)$(BUILD)/$(BIN)" "$(OK_COLOR)$(OK_STRING)$(NO_COLOR)\n"; \
        fi; \
        cat $@.log; \
        rm -f $@.log; \
        exit $$RESULT

$(BUILD)/%.o: %.cpp
	@$(CC) $(CFLAGS) -c $< -o $@ 2> $@.log; \
        RESULT=$$?; \
        if [ $$RESULT -ne 0 ]; then \
            printf "%-60b%b" "$(COM_COLOR)$(COM_STRING)$(OBJ_COLOR) $@" "$(ERROR_COLOR)$(ERROR_STRING)$(NO_COLOR)\n"; \
        elif [ -s $@.log ]; then \
            printf "%-60b%b" "$(COM_COLOR)$(COM_STRING)$(OBJ_COLOR) $@" "$(WARN_COLOR)$(WARN_STRING)$(NO_COLOR)\n"; \
        else  \
            printf "%-60b%b" "$(COM_COLOR)$(COM_STRING)$(OBJ_COLOR) $(@F)" "$(OK_COLOR)$(OK_STRING)$(NO_COLOR)\n"; \
        fi; \
	 cat $@.log; \
        rm -f $@.log; \
        exit $$RESULT

clean:
	@rm -fr $(BUILD)/* 2> $@.log; \
        RESULT=$$?; \
        if [ $$RESULT -ne 0 ]; then \
            printf "%-53b%b" "$(ERROR_COLOR)$(CL_STRING)" "$(ERROR_COLOR)$(ERROR_STRING)$(NO_COLOR)\n"; \
        elif [ -s $@.log ]; then \
            printf "%-53b%b" "$(ERROR_COLOR)$(CL_STRING)" "$(WARN_COLOR)$(WARN_STRING)$(NO_COLOR)\n"; \
        else  \
            printf "%-53b%b" "$(ERROR_COLOR)$(CL_STRING)" "$(OK_COLOR)$(OK_STRING)$(NO_COLOR)\n"; \
        fi; \
        cat $@.log; \
        rm -f $@.log; \
        exit $$RESULT

-include $(DEPS)
